# coding: utf-8

"""
    DigitalRebar Provision Server

    # An RestFUL API-driven Provisioner and DHCP server  # noqa: E501

    OpenAPI spec version: 4.14.0
    Contact: support@rackn.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six

from drppy_client.configuration import Configuration


class PoolResult(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'allocated': 'bool',
        'name': 'str',
        'status': 'PoolStatus',
        'uuid': 'str'
    }

    attribute_map = {
        'allocated': 'Allocated',
        'name': 'Name',
        'status': 'Status',
        'uuid': 'Uuid'
    }

    def __init__(self, allocated=None, name=None, status=None, uuid=None, _configuration=None):  # noqa: E501
        """PoolResult - a model defined in Swagger"""  # noqa: E501
        if _configuration is None:
            _configuration = Configuration()
        self._configuration = _configuration

        self._allocated = None
        self._name = None
        self._status = None
        self._uuid = None
        self.discriminator = None

        if allocated is not None:
            self.allocated = allocated
        if name is not None:
            self.name = name
        if status is not None:
            self.status = status
        if uuid is not None:
            self.uuid = uuid

    @property
    def allocated(self):
        """Gets the allocated of this PoolResult.  # noqa: E501

        Allocated status of the machine  # noqa: E501

        :return: The allocated of this PoolResult.  # noqa: E501
        :rtype: bool
        """
        return self._allocated

    @allocated.setter
    def allocated(self, allocated):
        """Sets the allocated of this PoolResult.

        Allocated status of the machine  # noqa: E501

        :param allocated: The allocated of this PoolResult.  # noqa: E501
        :type: bool
        """

        self._allocated = allocated

    @property
    def name(self):
        """Gets the name of this PoolResult.  # noqa: E501

        Name of the machine  # noqa: E501

        :return: The name of this PoolResult.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this PoolResult.

        Name of the machine  # noqa: E501

        :param name: The name of this PoolResult.  # noqa: E501
        :type: str
        """

        self._name = name

    @property
    def status(self):
        """Gets the status of this PoolResult.  # noqa: E501


        :return: The status of this PoolResult.  # noqa: E501
        :rtype: PoolStatus
        """
        return self._status

    @status.setter
    def status(self, status):
        """Sets the status of this PoolResult.


        :param status: The status of this PoolResult.  # noqa: E501
        :type: PoolStatus
        """

        self._status = status

    @property
    def uuid(self):
        """Gets the uuid of this PoolResult.  # noqa: E501

        UUID of the machine  # noqa: E501

        :return: The uuid of this PoolResult.  # noqa: E501
        :rtype: str
        """
        return self._uuid

    @uuid.setter
    def uuid(self, uuid):
        """Sets the uuid of this PoolResult.

        UUID of the machine  # noqa: E501

        :param uuid: The uuid of this PoolResult.  # noqa: E501
        :type: str
        """

        self._uuid = uuid

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(PoolResult, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, PoolResult):
            return False

        return self.to_dict() == other.to_dict()

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        if not isinstance(other, PoolResult):
            return True

        return self.to_dict() != other.to_dict()
