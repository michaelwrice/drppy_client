# UxOption

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**available** | **bool** | Available tracks whether or not the model passed validation. | [optional] 
**bundle** | **str** | Bundle tracks the name of the store containing this object. This field is read-only, and cannot be changed via the API. | [optional] 
**default** | **str** |  | [optional] 
**description** | **str** | Description is a short string description of the object | [optional] 
**documentation** | **str** | Documentation is an RST string defining the object | [optional] 
**endpoint** | **str** | Endpoint tracks the owner of the object among DRP endpoints | [optional] 
**errors** | **list[str]** | If there are any errors in the validation process, they will be available here. | [optional] 
**_global** | **bool** |  | [optional] 
**hidden** | **bool** |  | [optional] 
**id** | **str** | Id is the name of the object | [optional] 
**kind** | **str** |  | [optional] 
**params** | **dict(str, object)** | Params is an unused residual of the object from previous releases | [optional] 
**read_only** | **bool** | ReadOnly tracks if the store for this object is read-only. This flag is informational, and cannot be changed via the API. | [optional] 
**role** | **bool** |  | [optional] 
**user** | **bool** |  | [optional] 
**validated** | **bool** | Validated tracks whether or not the model has been validated. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


