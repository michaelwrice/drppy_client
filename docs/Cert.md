# Cert

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | **list[list[int]]** |  | [optional] 
**key** | [**PrivateKey**](PrivateKey.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


