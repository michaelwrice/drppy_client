# coding: utf-8

"""
    DigitalRebar Provision Server

    # An RestFUL API-driven Provisioner and DHCP server  # noqa: E501

    OpenAPI spec version: 4.14.0
    Contact: support@rackn.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six

from drppy_client.configuration import Configuration


class MachineFingerprint(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'csn_hash': 'str',
        'cloud_instance_id': 'str',
        'memory_ids': 'list[MemoryId]',
        'ssn_hash': 'str',
        'system_uuid': 'str'
    }

    attribute_map = {
        'csn_hash': 'CSNHash',
        'cloud_instance_id': 'CloudInstanceID',
        'memory_ids': 'MemoryIds',
        'ssn_hash': 'SSNHash',
        'system_uuid': 'SystemUUID'
    }

    def __init__(self, csn_hash=None, cloud_instance_id=None, memory_ids=None, ssn_hash=None, system_uuid=None, _configuration=None):  # noqa: E501
        """MachineFingerprint - a model defined in Swagger"""  # noqa: E501
        if _configuration is None:
            _configuration = Configuration()
        self._configuration = _configuration

        self._csn_hash = None
        self._cloud_instance_id = None
        self._memory_ids = None
        self._ssn_hash = None
        self._system_uuid = None
        self.discriminator = None

        if csn_hash is not None:
            self.csn_hash = csn_hash
        if cloud_instance_id is not None:
            self.cloud_instance_id = cloud_instance_id
        if memory_ids is not None:
            self.memory_ids = memory_ids
        if ssn_hash is not None:
            self.ssn_hash = ssn_hash
        if system_uuid is not None:
            self.system_uuid = system_uuid

    @property
    def csn_hash(self):
        """Gets the csn_hash of this MachineFingerprint.  # noqa: E501

        DMI.System.Manufacturer + DMI.System.ProductName + DMI.Chassis[0].SerialNumber, SHA256 hashed Hash must not be zero-length to match. 25 points  # noqa: E501

        :return: The csn_hash of this MachineFingerprint.  # noqa: E501
        :rtype: str
        """
        return self._csn_hash

    @csn_hash.setter
    def csn_hash(self, csn_hash):
        """Sets the csn_hash of this MachineFingerprint.

        DMI.System.Manufacturer + DMI.System.ProductName + DMI.Chassis[0].SerialNumber, SHA256 hashed Hash must not be zero-length to match. 25 points  # noqa: E501

        :param csn_hash: The csn_hash of this MachineFingerprint.  # noqa: E501
        :type: str
        """
        if (self._configuration.client_side_validation and
                csn_hash is not None and not re.search(r'^(?:[A-Za-z0-9+\/]{4})*(?:[A-Za-z0-9+\/]{2}==|[A-Za-z0-9+\/]{3}=)?$', csn_hash)):  # noqa: E501
            raise ValueError(r"Invalid value for `csn_hash`, must be a follow pattern or equal to `/^(?:[A-Za-z0-9+\/]{4})*(?:[A-Za-z0-9+\/]{2}==|[A-Za-z0-9+\/]{3}=)?$/`")  # noqa: E501

        self._csn_hash = csn_hash

    @property
    def cloud_instance_id(self):
        """Gets the cloud_instance_id of this MachineFingerprint.  # noqa: E501

        Cloud-init file in /run/cloud-init/instance-data.json String from ID of '.v1.cloud_name' + '.v1.instance_id'. 500 point match  # noqa: E501

        :return: The cloud_instance_id of this MachineFingerprint.  # noqa: E501
        :rtype: str
        """
        return self._cloud_instance_id

    @cloud_instance_id.setter
    def cloud_instance_id(self, cloud_instance_id):
        """Sets the cloud_instance_id of this MachineFingerprint.

        Cloud-init file in /run/cloud-init/instance-data.json String from ID of '.v1.cloud_name' + '.v1.instance_id'. 500 point match  # noqa: E501

        :param cloud_instance_id: The cloud_instance_id of this MachineFingerprint.  # noqa: E501
        :type: str
        """

        self._cloud_instance_id = cloud_instance_id

    @property
    def memory_ids(self):
        """Gets the memory_ids of this MachineFingerprint.  # noqa: E501

        MemoryIds is an array of SHA256sums if the following fields in each entry of the DMI.Memory.Devices array concatenated together: Manufacturer PartNumber SerialNumber Each hash must not be zero length Score is % matched.  # noqa: E501

        :return: The memory_ids of this MachineFingerprint.  # noqa: E501
        :rtype: list[MemoryId]
        """
        return self._memory_ids

    @memory_ids.setter
    def memory_ids(self, memory_ids):
        """Sets the memory_ids of this MachineFingerprint.

        MemoryIds is an array of SHA256sums if the following fields in each entry of the DMI.Memory.Devices array concatenated together: Manufacturer PartNumber SerialNumber Each hash must not be zero length Score is % matched.  # noqa: E501

        :param memory_ids: The memory_ids of this MachineFingerprint.  # noqa: E501
        :type: list[MemoryId]
        """

        self._memory_ids = memory_ids

    @property
    def ssn_hash(self):
        """Gets the ssn_hash of this MachineFingerprint.  # noqa: E501

        DMI.System.Manufacturer + DMI.System.ProductName + DMI.System.SerialNumber, SHA256 hashed Hash must not be zero-length to match. 25 points  # noqa: E501

        :return: The ssn_hash of this MachineFingerprint.  # noqa: E501
        :rtype: str
        """
        return self._ssn_hash

    @ssn_hash.setter
    def ssn_hash(self, ssn_hash):
        """Sets the ssn_hash of this MachineFingerprint.

        DMI.System.Manufacturer + DMI.System.ProductName + DMI.System.SerialNumber, SHA256 hashed Hash must not be zero-length to match. 25 points  # noqa: E501

        :param ssn_hash: The ssn_hash of this MachineFingerprint.  # noqa: E501
        :type: str
        """
        if (self._configuration.client_side_validation and
                ssn_hash is not None and not re.search(r'^(?:[A-Za-z0-9+\/]{4})*(?:[A-Za-z0-9+\/]{2}==|[A-Za-z0-9+\/]{3}=)?$', ssn_hash)):  # noqa: E501
            raise ValueError(r"Invalid value for `ssn_hash`, must be a follow pattern or equal to `/^(?:[A-Za-z0-9+\/]{4})*(?:[A-Za-z0-9+\/]{2}==|[A-Za-z0-9+\/]{3}=)?$/`")  # noqa: E501

        self._ssn_hash = ssn_hash

    @property
    def system_uuid(self):
        """Gets the system_uuid of this MachineFingerprint.  # noqa: E501

        DMI.System.UUID, not hashed. Must be non zero length and must be a non-zero UUID. 50 point match  # noqa: E501

        :return: The system_uuid of this MachineFingerprint.  # noqa: E501
        :rtype: str
        """
        return self._system_uuid

    @system_uuid.setter
    def system_uuid(self, system_uuid):
        """Sets the system_uuid of this MachineFingerprint.

        DMI.System.UUID, not hashed. Must be non zero length and must be a non-zero UUID. 50 point match  # noqa: E501

        :param system_uuid: The system_uuid of this MachineFingerprint.  # noqa: E501
        :type: str
        """

        self._system_uuid = system_uuid

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(MachineFingerprint, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, MachineFingerprint):
            return False

        return self.to_dict() == other.to_dict()

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        if not isinstance(other, MachineFingerprint):
            return True

        return self.to_dict() != other.to_dict()
