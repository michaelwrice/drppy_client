# Action

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**command** | **str** |  | [optional] 
**command_set** | **str** |  | [optional] 
**job** | **str** |  | [optional] 
**model** | **object** |  | [optional] 
**params** | **dict(str, object)** |  | [optional] 
**plugin** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


