# coding: utf-8

"""
    DigitalRebar Provision Server

    An RestFUL API-driven Provisioner and DHCP server  # noqa: E501

    OpenAPI spec version: 4.0.0
    Contact: greg@rackn.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import unittest

import drppy_client
from drppy_client.api.objects_api import ObjectsApi  # noqa: E501
from drppy_client.rest import ApiException


class TestObjectsApi(unittest.TestCase):
    """ObjectsApi unit test stubs"""

    def setUp(self):
        self.api = drppy_client.api.objects_api.ObjectsApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_list_objects(self):
        """Test case for list_objects

        """
        pass


if __name__ == '__main__':
    unittest.main()
