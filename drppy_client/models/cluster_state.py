# coding: utf-8

"""
    DigitalRebar Provision Server

    # An RestFUL API-driven Provisioner and DHCP server  # noqa: E501

    OpenAPI spec version: 4.14.0
    Contact: support@rackn.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six

from drppy_client.configuration import Configuration


class ClusterState(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'active_uri': 'str',
        'consensus_enabled': 'bool',
        'consensus_join': 'str',
        'enabled': 'bool',
        'ha_id': 'str',
        'load_balanced': 'bool',
        'nodes': 'list[NodeInfo]',
        'roots': 'list[Cert]',
        'server_hostname': 'str',
        'token': 'str',
        'valid': 'bool',
        'virt_addr': 'str'
    }

    attribute_map = {
        'active_uri': 'ActiveUri',
        'consensus_enabled': 'ConsensusEnabled',
        'consensus_join': 'ConsensusJoin',
        'enabled': 'Enabled',
        'ha_id': 'HaID',
        'load_balanced': 'LoadBalanced',
        'nodes': 'Nodes',
        'roots': 'Roots',
        'server_hostname': 'ServerHostname',
        'token': 'Token',
        'valid': 'Valid',
        'virt_addr': 'VirtAddr'
    }

    def __init__(self, active_uri=None, consensus_enabled=None, consensus_join=None, enabled=None, ha_id=None, load_balanced=None, nodes=None, roots=None, server_hostname=None, token=None, valid=None, virt_addr=None, _configuration=None):  # noqa: E501
        """ClusterState - a model defined in Swagger"""  # noqa: E501
        if _configuration is None:
            _configuration = Configuration()
        self._configuration = _configuration

        self._active_uri = None
        self._consensus_enabled = None
        self._consensus_join = None
        self._enabled = None
        self._ha_id = None
        self._load_balanced = None
        self._nodes = None
        self._roots = None
        self._server_hostname = None
        self._token = None
        self._valid = None
        self._virt_addr = None
        self.discriminator = None

        if active_uri is not None:
            self.active_uri = active_uri
        if consensus_enabled is not None:
            self.consensus_enabled = consensus_enabled
        if consensus_join is not None:
            self.consensus_join = consensus_join
        if enabled is not None:
            self.enabled = enabled
        if ha_id is not None:
            self.ha_id = ha_id
        if load_balanced is not None:
            self.load_balanced = load_balanced
        if nodes is not None:
            self.nodes = nodes
        if roots is not None:
            self.roots = roots
        if server_hostname is not None:
            self.server_hostname = server_hostname
        if token is not None:
            self.token = token
        if valid is not None:
            self.valid = valid
        if virt_addr is not None:
            self.virt_addr = virt_addr

    @property
    def active_uri(self):
        """Gets the active_uri of this ClusterState.  # noqa: E501

        ActiveUri is the API URL of cthe cluster as built from the virtual addr.  # noqa: E501

        :return: The active_uri of this ClusterState.  # noqa: E501
        :rtype: str
        """
        return self._active_uri

    @active_uri.setter
    def active_uri(self, active_uri):
        """Sets the active_uri of this ClusterState.

        ActiveUri is the API URL of cthe cluster as built from the virtual addr.  # noqa: E501

        :param active_uri: The active_uri of this ClusterState.  # noqa: E501
        :type: str
        """

        self._active_uri = active_uri

    @property
    def consensus_enabled(self):
        """Gets the consensus_enabled of this ClusterState.  # noqa: E501

        ConsensusEnabled indicates that this cluster is operating on the Raft based replication protocol with automatic failover.  # noqa: E501

        :return: The consensus_enabled of this ClusterState.  # noqa: E501
        :rtype: bool
        """
        return self._consensus_enabled

    @consensus_enabled.setter
    def consensus_enabled(self, consensus_enabled):
        """Sets the consensus_enabled of this ClusterState.

        ConsensusEnabled indicates that this cluster is operating on the Raft based replication protocol with automatic failover.  # noqa: E501

        :param consensus_enabled: The consensus_enabled of this ClusterState.  # noqa: E501
        :type: bool
        """

        self._consensus_enabled = consensus_enabled

    @property
    def consensus_join(self):
        """Gets the consensus_join of this ClusterState.  # noqa: E501

        ConsensusJoin is the API URL of the current active node in a consensus cluster.  It may be unset if the cluster nodes cannot agree who should be the active node, or if the cluster is operating using the sync replication protocol.  # noqa: E501

        :return: The consensus_join of this ClusterState.  # noqa: E501
        :rtype: str
        """
        return self._consensus_join

    @consensus_join.setter
    def consensus_join(self, consensus_join):
        """Sets the consensus_join of this ClusterState.

        ConsensusJoin is the API URL of the current active node in a consensus cluster.  It may be unset if the cluster nodes cannot agree who should be the active node, or if the cluster is operating using the sync replication protocol.  # noqa: E501

        :param consensus_join: The consensus_join of this ClusterState.  # noqa: E501
        :type: str
        """

        self._consensus_join = consensus_join

    @property
    def enabled(self):
        """Gets the enabled of this ClusterState.  # noqa: E501

        Enabled indicates whether either HA mode is operating on this cluster. If just Enabled is set, the cluster is using the synchronous replication protocol with manual failover.  # noqa: E501

        :return: The enabled of this ClusterState.  # noqa: E501
        :rtype: bool
        """
        return self._enabled

    @enabled.setter
    def enabled(self, enabled):
        """Sets the enabled of this ClusterState.

        Enabled indicates whether either HA mode is operating on this cluster. If just Enabled is set, the cluster is using the synchronous replication protocol with manual failover.  # noqa: E501

        :param enabled: The enabled of this ClusterState.  # noqa: E501
        :type: bool
        """

        self._enabled = enabled

    @property
    def ha_id(self):
        """Gets the ha_id of this ClusterState.  # noqa: E501

        HaID is the ID of the cluster as a whole.  # noqa: E501

        :return: The ha_id of this ClusterState.  # noqa: E501
        :rtype: str
        """
        return self._ha_id

    @ha_id.setter
    def ha_id(self, ha_id):
        """Sets the ha_id of this ClusterState.

        HaID is the ID of the cluster as a whole.  # noqa: E501

        :param ha_id: The ha_id of this ClusterState.  # noqa: E501
        :type: str
        """

        self._ha_id = ha_id

    @property
    def load_balanced(self):
        """Gets the load_balanced of this ClusterState.  # noqa: E501

        LoadBalanced indicates that an external service is responsible for routing traffic destined to VirtAddr to a cluster node.  # noqa: E501

        :return: The load_balanced of this ClusterState.  # noqa: E501
        :rtype: bool
        """
        return self._load_balanced

    @load_balanced.setter
    def load_balanced(self, load_balanced):
        """Sets the load_balanced of this ClusterState.

        LoadBalanced indicates that an external service is responsible for routing traffic destined to VirtAddr to a cluster node.  # noqa: E501

        :param load_balanced: The load_balanced of this ClusterState.  # noqa: E501
        :type: bool
        """

        self._load_balanced = load_balanced

    @property
    def nodes(self):
        """Gets the nodes of this ClusterState.  # noqa: E501


        :return: The nodes of this ClusterState.  # noqa: E501
        :rtype: list[NodeInfo]
        """
        return self._nodes

    @nodes.setter
    def nodes(self, nodes):
        """Sets the nodes of this ClusterState.


        :param nodes: The nodes of this ClusterState.  # noqa: E501
        :type: list[NodeInfo]
        """

        self._nodes = nodes

    @property
    def roots(self):
        """Gets the roots of this ClusterState.  # noqa: E501

        Roots is a list of self-signed trust roots that consensus nodes will use to verify communication.  These roots are automatically created and rotated on a regular basis.  # noqa: E501

        :return: The roots of this ClusterState.  # noqa: E501
        :rtype: list[Cert]
        """
        return self._roots

    @roots.setter
    def roots(self, roots):
        """Sets the roots of this ClusterState.

        Roots is a list of self-signed trust roots that consensus nodes will use to verify communication.  These roots are automatically created and rotated on a regular basis.  # noqa: E501

        :param roots: The roots of this ClusterState.  # noqa: E501
        :type: list[Cert]
        """

        self._roots = roots

    @property
    def server_hostname(self):
        """Gets the server_hostname of this ClusterState.  # noqa: E501

        ServerHostname is the DNS name for the DRP endpoint that managed systems should use.  # noqa: E501

        :return: The server_hostname of this ClusterState.  # noqa: E501
        :rtype: str
        """
        return self._server_hostname

    @server_hostname.setter
    def server_hostname(self, server_hostname):
        """Sets the server_hostname of this ClusterState.

        ServerHostname is the DNS name for the DRP endpoint that managed systems should use.  # noqa: E501

        :param server_hostname: The server_hostname of this ClusterState.  # noqa: E501
        :type: str
        """

        self._server_hostname = server_hostname

    @property
    def token(self):
        """Gets the token of this ClusterState.  # noqa: E501

        Token is an API authentication token that can be sued to perform cluster operations.  # noqa: E501

        :return: The token of this ClusterState.  # noqa: E501
        :rtype: str
        """
        return self._token

    @token.setter
    def token(self, token):
        """Sets the token of this ClusterState.

        Token is an API authentication token that can be sued to perform cluster operations.  # noqa: E501

        :param token: The token of this ClusterState.  # noqa: E501
        :type: str
        """

        self._token = token

    @property
    def valid(self):
        """Gets the valid of this ClusterState.  # noqa: E501

        Valid indicates that this state is valid and has been consistency checked.  # noqa: E501

        :return: The valid of this ClusterState.  # noqa: E501
        :rtype: bool
        """
        return self._valid

    @valid.setter
    def valid(self, valid):
        """Sets the valid of this ClusterState.

        Valid indicates that this state is valid and has been consistency checked.  # noqa: E501

        :param valid: The valid of this ClusterState.  # noqa: E501
        :type: bool
        """

        self._valid = valid

    @property
    def virt_addr(self):
        """Gets the virt_addr of this ClusterState.  # noqa: E501

        VirtAddr is the IP address that the cluster should appear to have from the perspective of clients and third parties.  # noqa: E501

        :return: The virt_addr of this ClusterState.  # noqa: E501
        :rtype: str
        """
        return self._virt_addr

    @virt_addr.setter
    def virt_addr(self, virt_addr):
        """Sets the virt_addr of this ClusterState.

        VirtAddr is the IP address that the cluster should appear to have from the perspective of clients and third parties.  # noqa: E501

        :param virt_addr: The virt_addr of this ClusterState.  # noqa: E501
        :type: str
        """

        self._virt_addr = virt_addr

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(ClusterState, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, ClusterState):
            return False

        return self.to_dict() == other.to_dict()

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        if not isinstance(other, ClusterState):
            return True

        return self.to_dict() != other.to_dict()
