# PoolTransitionActions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**add_parameters** | **dict(str, object)** |  | [optional] 
**add_profiles** | **list[str]** |  | [optional] 
**remove_parameters** | **list[str]** |  | [optional] 
**remove_profiles** | **list[str]** |  | [optional] 
**workflow** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


