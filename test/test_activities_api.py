# coding: utf-8

"""
    DigitalRebar Provision Server

    An RestFUL API-driven Provisioner and DHCP server  # noqa: E501

    OpenAPI spec version: 4.0.0
    Contact: greg@rackn.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import unittest

import drppy_client
from drppy_client.api.activities_api import ActivitiesApi  # noqa: E501
from drppy_client.rest import ApiException


class TestActivitiesApi(unittest.TestCase):
    """ActivitiesApi unit test stubs"""

    def setUp(self):
        self.api = drppy_client.api.activities_api.ActivitiesApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_get_activity(self):
        """Test case for get_activity

        Get a Activity  # noqa: E501
        """
        pass

    def test_get_activity_action(self):
        """Test case for get_activity_action

        List specific action for a activity Activity  # noqa: E501
        """
        pass

    def test_get_activity_actions(self):
        """Test case for get_activity_actions

        List activity actions Activity  # noqa: E501
        """
        pass

    def test_head_activity(self):
        """Test case for head_activity

        See if a Activity exists  # noqa: E501
        """
        pass

    def test_list_activities(self):
        """Test case for list_activities

        Lists Activities filtered by some parameters.  # noqa: E501
        """
        pass

    def test_list_stats_activities(self):
        """Test case for list_stats_activities

        Stats of the List Activities filtered by some parameters.  # noqa: E501
        """
        pass

    def test_post_activity_action(self):
        """Test case for post_activity_action

        Call an action on the node.  # noqa: E501
        """
        pass


if __name__ == '__main__':
    unittest.main()
