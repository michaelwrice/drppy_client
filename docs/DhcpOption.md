# DhcpOption

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **int** | Code is a DHCP Option Code. | 
**value** | **str** | Value is a text/template that will be expanded and then converted into the proper format for the option code | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


