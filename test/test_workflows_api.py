# coding: utf-8

"""
    DigitalRebar Provision Server

    An RestFUL API-driven Provisioner and DHCP server  # noqa: E501

    OpenAPI spec version: 4.0.0
    Contact: greg@rackn.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import unittest

import drppy_client
from drppy_client.api.workflows_api import WorkflowsApi  # noqa: E501
from drppy_client.rest import ApiException


class TestWorkflowsApi(unittest.TestCase):
    """WorkflowsApi unit test stubs"""

    def setUp(self):
        self.api = drppy_client.api.workflows_api.WorkflowsApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_create_workflow(self):
        """Test case for create_workflow

        Create a Workflow  # noqa: E501
        """
        pass

    def test_delete_workflow(self):
        """Test case for delete_workflow

        Delete a Workflow  # noqa: E501
        """
        pass

    def test_get_workflow(self):
        """Test case for get_workflow

        Get a Workflow  # noqa: E501
        """
        pass

    def test_get_workflow_action(self):
        """Test case for get_workflow_action

        List specific action for a workflow Workflow  # noqa: E501
        """
        pass

    def test_get_workflow_actions(self):
        """Test case for get_workflow_actions

        List workflow actions Workflow  # noqa: E501
        """
        pass

    def test_head_workflow(self):
        """Test case for head_workflow

        See if a Workflow exists  # noqa: E501
        """
        pass

    def test_list_stats_workflows(self):
        """Test case for list_stats_workflows

        Stats of the List Workflows filtered by some parameters.  # noqa: E501
        """
        pass

    def test_list_workflows(self):
        """Test case for list_workflows

        Lists Workflows filtered by some parameters.  # noqa: E501
        """
        pass

    def test_patch_workflow(self):
        """Test case for patch_workflow

        Patch a Workflow  # noqa: E501
        """
        pass

    def test_post_workflow_action(self):
        """Test case for post_workflow_action

        Call an action on the node.  # noqa: E501
        """
        pass

    def test_put_workflow(self):
        """Test case for put_workflow

        Put a Workflow  # noqa: E501
        """
        pass


if __name__ == '__main__':
    unittest.main()
