# AvailableAction

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**command** | **str** |  | [optional] 
**model** | **str** |  | [optional] 
**optional_params** | **list[str]** |  | [optional] 
**provider** | **str** |  | [optional] 
**required_params** | **list[str]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


