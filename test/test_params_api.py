# coding: utf-8

"""
    DigitalRebar Provision Server

    An RestFUL API-driven Provisioner and DHCP server  # noqa: E501

    OpenAPI spec version: 4.0.0
    Contact: greg@rackn.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import unittest

import drppy_client
from drppy_client.api.params_api import ParamsApi  # noqa: E501
from drppy_client.rest import ApiException


class TestParamsApi(unittest.TestCase):
    """ParamsApi unit test stubs"""

    def setUp(self):
        self.api = drppy_client.api.params_api.ParamsApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_create_param(self):
        """Test case for create_param

        Create a Param  # noqa: E501
        """
        pass

    def test_delete_param(self):
        """Test case for delete_param

        Delete a Param  # noqa: E501
        """
        pass

    def test_get_param(self):
        """Test case for get_param

        Get a Param  # noqa: E501
        """
        pass

    def test_head_param(self):
        """Test case for head_param

        See if a Param exists  # noqa: E501
        """
        pass

    def test_list_params(self):
        """Test case for list_params

        Lists Params filtered by some parameters.  # noqa: E501
        """
        pass

    def test_list_stats_params(self):
        """Test case for list_stats_params

        Stats of the List Params filtered by some parameters.  # noqa: E501
        """
        pass

    def test_patch_param(self):
        """Test case for patch_param

        Patch a Param  # noqa: E501
        """
        pass

    def test_put_param(self):
        """Test case for put_param

        Put a Param  # noqa: E501
        """
        pass


if __name__ == '__main__':
    unittest.main()
