# Query

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | Name is the field / parameter to lookup | [optional] 
**op** | **str** | Op is the operation to apply to the field Options are &#x3D;,Eq,Re,Nin,In,Gt,... | [optional] 
**value** | **str** | Value is the string value of the item to test. The operator will convert to the field type as needed. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


