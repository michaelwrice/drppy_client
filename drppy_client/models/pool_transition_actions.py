# coding: utf-8

"""
    DigitalRebar Provision Server

    # An RestFUL API-driven Provisioner and DHCP server  # noqa: E501

    OpenAPI spec version: 4.14.0
    Contact: support@rackn.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six

from drppy_client.configuration import Configuration


class PoolTransitionActions(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'add_parameters': 'dict(str, object)',
        'add_profiles': 'list[str]',
        'remove_parameters': 'list[str]',
        'remove_profiles': 'list[str]',
        'workflow': 'str'
    }

    attribute_map = {
        'add_parameters': 'AddParameters',
        'add_profiles': 'AddProfiles',
        'remove_parameters': 'RemoveParameters',
        'remove_profiles': 'RemoveProfiles',
        'workflow': 'Workflow'
    }

    def __init__(self, add_parameters=None, add_profiles=None, remove_parameters=None, remove_profiles=None, workflow=None, _configuration=None):  # noqa: E501
        """PoolTransitionActions - a model defined in Swagger"""  # noqa: E501
        if _configuration is None:
            _configuration = Configuration()
        self._configuration = _configuration

        self._add_parameters = None
        self._add_profiles = None
        self._remove_parameters = None
        self._remove_profiles = None
        self._workflow = None
        self.discriminator = None

        if add_parameters is not None:
            self.add_parameters = add_parameters
        if add_profiles is not None:
            self.add_profiles = add_profiles
        if remove_parameters is not None:
            self.remove_parameters = remove_parameters
        if remove_profiles is not None:
            self.remove_profiles = remove_profiles
        if workflow is not None:
            self.workflow = workflow

    @property
    def add_parameters(self):
        """Gets the add_parameters of this PoolTransitionActions.  # noqa: E501

        AddParameters defines a list of parameters to add to the machine  # noqa: E501

        :return: The add_parameters of this PoolTransitionActions.  # noqa: E501
        :rtype: dict(str, object)
        """
        return self._add_parameters

    @add_parameters.setter
    def add_parameters(self, add_parameters):
        """Sets the add_parameters of this PoolTransitionActions.

        AddParameters defines a list of parameters to add to the machine  # noqa: E501

        :param add_parameters: The add_parameters of this PoolTransitionActions.  # noqa: E501
        :type: dict(str, object)
        """

        self._add_parameters = add_parameters

    @property
    def add_profiles(self):
        """Gets the add_profiles of this PoolTransitionActions.  # noqa: E501

        AddProfiles defines a list of profiles to add to the machine  # noqa: E501

        :return: The add_profiles of this PoolTransitionActions.  # noqa: E501
        :rtype: list[str]
        """
        return self._add_profiles

    @add_profiles.setter
    def add_profiles(self, add_profiles):
        """Sets the add_profiles of this PoolTransitionActions.

        AddProfiles defines a list of profiles to add to the machine  # noqa: E501

        :param add_profiles: The add_profiles of this PoolTransitionActions.  # noqa: E501
        :type: list[str]
        """

        self._add_profiles = add_profiles

    @property
    def remove_parameters(self):
        """Gets the remove_parameters of this PoolTransitionActions.  # noqa: E501

        RemoveParameters defines a list of parameters to remove from the machine  # noqa: E501

        :return: The remove_parameters of this PoolTransitionActions.  # noqa: E501
        :rtype: list[str]
        """
        return self._remove_parameters

    @remove_parameters.setter
    def remove_parameters(self, remove_parameters):
        """Sets the remove_parameters of this PoolTransitionActions.

        RemoveParameters defines a list of parameters to remove from the machine  # noqa: E501

        :param remove_parameters: The remove_parameters of this PoolTransitionActions.  # noqa: E501
        :type: list[str]
        """

        self._remove_parameters = remove_parameters

    @property
    def remove_profiles(self):
        """Gets the remove_profiles of this PoolTransitionActions.  # noqa: E501

        RemoveProfiles defines a list of profiles to remove from the machine  # noqa: E501

        :return: The remove_profiles of this PoolTransitionActions.  # noqa: E501
        :rtype: list[str]
        """
        return self._remove_profiles

    @remove_profiles.setter
    def remove_profiles(self, remove_profiles):
        """Sets the remove_profiles of this PoolTransitionActions.

        RemoveProfiles defines a list of profiles to remove from the machine  # noqa: E501

        :param remove_profiles: The remove_profiles of this PoolTransitionActions.  # noqa: E501
        :type: list[str]
        """

        self._remove_profiles = remove_profiles

    @property
    def workflow(self):
        """Gets the workflow of this PoolTransitionActions.  # noqa: E501

        Workflow defines the new workflow the machine should run  # noqa: E501

        :return: The workflow of this PoolTransitionActions.  # noqa: E501
        :rtype: str
        """
        return self._workflow

    @workflow.setter
    def workflow(self, workflow):
        """Sets the workflow of this PoolTransitionActions.

        Workflow defines the new workflow the machine should run  # noqa: E501

        :param workflow: The workflow of this PoolTransitionActions.  # noqa: E501
        :type: str
        """

        self._workflow = workflow

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(PoolTransitionActions, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, PoolTransitionActions):
            return False

        return self.to_dict() == other.to_dict()

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        if not isinstance(other, PoolTransitionActions):
            return True

        return self.to_dict() != other.to_dict()
