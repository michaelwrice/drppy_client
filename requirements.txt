certifi>=14.05.14               # SSL certificates
six>=1.10                       # Python 2/3 compatibility
python-dateutil>=2.5.3          # Date/time utilities
urllib3>=1.15.1                 # HTTP client
click>=8.1.7                    # Command-line utilities
pynacl>=1.5                     # Encryption utilities
