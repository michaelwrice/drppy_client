# coding: utf-8

"""
    DigitalRebar Provision Server

    # An RestFUL API-driven Provisioner and DHCP server  # noqa: E501

    OpenAPI spec version: 4.14.0
    Contact: support@rackn.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six

from drppy_client.configuration import Configuration


class UxOption(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'available': 'bool',
        'bundle': 'str',
        'created_at': 'datetime',
        'created_by': 'str',
        'default': 'str',
        'description': 'str',
        'documentation': 'str',
        'endpoint': 'str',
        'errors': 'list[str]',
        '_global': 'bool',
        'hidden': 'bool',
        'id': 'str',
        'kind': 'str',
        'last_modified_at': 'datetime',
        'last_modified_by': 'str',
        'meta': 'Meta',
        'params': 'dict(str, object)',
        'read_only': 'bool',
        'role': 'bool',
        'user': 'bool',
        'validated': 'bool'
    }

    attribute_map = {
        'available': 'Available',
        'bundle': 'Bundle',
        'created_at': 'CreatedAt',
        'created_by': 'CreatedBy',
        'default': 'Default',
        'description': 'Description',
        'documentation': 'Documentation',
        'endpoint': 'Endpoint',
        'errors': 'Errors',
        '_global': 'Global',
        'hidden': 'Hidden',
        'id': 'Id',
        'kind': 'Kind',
        'last_modified_at': 'LastModifiedAt',
        'last_modified_by': 'LastModifiedBy',
        'meta': 'Meta',
        'params': 'Params',
        'read_only': 'ReadOnly',
        'role': 'Role',
        'user': 'User',
        'validated': 'Validated'
    }

    def __init__(self, available=None, bundle=None, created_at=None, created_by=None, default=None, description=None, documentation=None, endpoint=None, errors=None, _global=None, hidden=None, id=None, kind=None, last_modified_at=None, last_modified_by=None, meta=None, params=None, read_only=None, role=None, user=None, validated=None, _configuration=None):  # noqa: E501
        """UxOption - a model defined in Swagger"""  # noqa: E501
        if _configuration is None:
            _configuration = Configuration()
        self._configuration = _configuration

        self._available = None
        self._bundle = None
        self._created_at = None
        self._created_by = None
        self._default = None
        self._description = None
        self._documentation = None
        self._endpoint = None
        self._errors = None
        self.__global = None
        self._hidden = None
        self._id = None
        self._kind = None
        self._last_modified_at = None
        self._last_modified_by = None
        self._meta = None
        self._params = None
        self._read_only = None
        self._role = None
        self._user = None
        self._validated = None
        self.discriminator = None

        if available is not None:
            self.available = available
        if bundle is not None:
            self.bundle = bundle
        if created_at is not None:
            self.created_at = created_at
        if created_by is not None:
            self.created_by = created_by
        if default is not None:
            self.default = default
        if description is not None:
            self.description = description
        if documentation is not None:
            self.documentation = documentation
        if endpoint is not None:
            self.endpoint = endpoint
        if errors is not None:
            self.errors = errors
        if _global is not None:
            self._global = _global
        if hidden is not None:
            self.hidden = hidden
        if id is not None:
            self.id = id
        if kind is not None:
            self.kind = kind
        if last_modified_at is not None:
            self.last_modified_at = last_modified_at
        if last_modified_by is not None:
            self.last_modified_by = last_modified_by
        if meta is not None:
            self.meta = meta
        if params is not None:
            self.params = params
        if read_only is not None:
            self.read_only = read_only
        if role is not None:
            self.role = role
        if user is not None:
            self.user = user
        if validated is not None:
            self.validated = validated

    @property
    def available(self):
        """Gets the available of this UxOption.  # noqa: E501

        Available tracks whether or not the model passed validation.  # noqa: E501

        :return: The available of this UxOption.  # noqa: E501
        :rtype: bool
        """
        return self._available

    @available.setter
    def available(self, available):
        """Sets the available of this UxOption.

        Available tracks whether or not the model passed validation.  # noqa: E501

        :param available: The available of this UxOption.  # noqa: E501
        :type: bool
        """

        self._available = available

    @property
    def bundle(self):
        """Gets the bundle of this UxOption.  # noqa: E501

        Bundle tracks the name of the store containing this object. This field is read-only, and cannot be changed via the API.  # noqa: E501

        :return: The bundle of this UxOption.  # noqa: E501
        :rtype: str
        """
        return self._bundle

    @bundle.setter
    def bundle(self, bundle):
        """Sets the bundle of this UxOption.

        Bundle tracks the name of the store containing this object. This field is read-only, and cannot be changed via the API.  # noqa: E501

        :param bundle: The bundle of this UxOption.  # noqa: E501
        :type: str
        """

        self._bundle = bundle

    @property
    def created_at(self):
        """Gets the created_at of this UxOption.  # noqa: E501

        CreatedAt is the time that this object was created.  # noqa: E501

        :return: The created_at of this UxOption.  # noqa: E501
        :rtype: datetime
        """
        return self._created_at

    @created_at.setter
    def created_at(self, created_at):
        """Sets the created_at of this UxOption.

        CreatedAt is the time that this object was created.  # noqa: E501

        :param created_at: The created_at of this UxOption.  # noqa: E501
        :type: datetime
        """

        self._created_at = created_at

    @property
    def created_by(self):
        """Gets the created_by of this UxOption.  # noqa: E501

        CreatedBy stores the value of the user that created this object. Note: This value is stored ONLY if the object was created by a user which means that `currentUserName` needs to be populated in the authBlob  # noqa: E501

        :return: The created_by of this UxOption.  # noqa: E501
        :rtype: str
        """
        return self._created_by

    @created_by.setter
    def created_by(self, created_by):
        """Sets the created_by of this UxOption.

        CreatedBy stores the value of the user that created this object. Note: This value is stored ONLY if the object was created by a user which means that `currentUserName` needs to be populated in the authBlob  # noqa: E501

        :param created_by: The created_by of this UxOption.  # noqa: E501
        :type: str
        """

        self._created_by = created_by

    @property
    def default(self):
        """Gets the default of this UxOption.  # noqa: E501

        Default is the default value  # noqa: E501

        :return: The default of this UxOption.  # noqa: E501
        :rtype: str
        """
        return self._default

    @default.setter
    def default(self, default):
        """Sets the default of this UxOption.

        Default is the default value  # noqa: E501

        :param default: The default of this UxOption.  # noqa: E501
        :type: str
        """

        self._default = default

    @property
    def description(self):
        """Gets the description of this UxOption.  # noqa: E501

        Description is a string for providing a simple description  # noqa: E501

        :return: The description of this UxOption.  # noqa: E501
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description):
        """Sets the description of this UxOption.

        Description is a string for providing a simple description  # noqa: E501

        :param description: The description of this UxOption.  # noqa: E501
        :type: str
        """

        self._description = description

    @property
    def documentation(self):
        """Gets the documentation of this UxOption.  # noqa: E501

        Documentation is a string for providing additional in depth information.  # noqa: E501

        :return: The documentation of this UxOption.  # noqa: E501
        :rtype: str
        """
        return self._documentation

    @documentation.setter
    def documentation(self, documentation):
        """Sets the documentation of this UxOption.

        Documentation is a string for providing additional in depth information.  # noqa: E501

        :param documentation: The documentation of this UxOption.  # noqa: E501
        :type: str
        """

        self._documentation = documentation

    @property
    def endpoint(self):
        """Gets the endpoint of this UxOption.  # noqa: E501

        Endpoint tracks the owner of the object among DRP endpoints  # noqa: E501

        :return: The endpoint of this UxOption.  # noqa: E501
        :rtype: str
        """
        return self._endpoint

    @endpoint.setter
    def endpoint(self, endpoint):
        """Sets the endpoint of this UxOption.

        Endpoint tracks the owner of the object among DRP endpoints  # noqa: E501

        :param endpoint: The endpoint of this UxOption.  # noqa: E501
        :type: str
        """

        self._endpoint = endpoint

    @property
    def errors(self):
        """Gets the errors of this UxOption.  # noqa: E501

        If there are any errors in the validation process, they will be available here.  # noqa: E501

        :return: The errors of this UxOption.  # noqa: E501
        :rtype: list[str]
        """
        return self._errors

    @errors.setter
    def errors(self, errors):
        """Sets the errors of this UxOption.

        If there are any errors in the validation process, they will be available here.  # noqa: E501

        :param errors: The errors of this UxOption.  # noqa: E501
        :type: list[str]
        """

        self._errors = errors

    @property
    def _global(self):
        """Gets the _global of this UxOption.  # noqa: E501

        Global defines if it is a global scoped option  # noqa: E501

        :return: The _global of this UxOption.  # noqa: E501
        :rtype: bool
        """
        return self.__global

    @_global.setter
    def _global(self, _global):
        """Sets the _global of this UxOption.

        Global defines if it is a global scoped option  # noqa: E501

        :param _global: The _global of this UxOption.  # noqa: E501
        :type: bool
        """

        self.__global = _global

    @property
    def hidden(self):
        """Gets the hidden of this UxOption.  # noqa: E501

        Hiddend defines if it is hidden from the UX  # noqa: E501

        :return: The hidden of this UxOption.  # noqa: E501
        :rtype: bool
        """
        return self._hidden

    @hidden.setter
    def hidden(self, hidden):
        """Sets the hidden of this UxOption.

        Hiddend defines if it is hidden from the UX  # noqa: E501

        :param hidden: The hidden of this UxOption.  # noqa: E501
        :type: bool
        """

        self._hidden = hidden

    @property
    def id(self):
        """Gets the id of this UxOption.  # noqa: E501

        Id is the name of the object  # noqa: E501

        :return: The id of this UxOption.  # noqa: E501
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this UxOption.

        Id is the name of the object  # noqa: E501

        :param id: The id of this UxOption.  # noqa: E501
        :type: str
        """

        self._id = id

    @property
    def kind(self):
        """Gets the kind of this UxOption.  # noqa: E501

        Kind defines type of option  # noqa: E501

        :return: The kind of this UxOption.  # noqa: E501
        :rtype: str
        """
        return self._kind

    @kind.setter
    def kind(self, kind):
        """Sets the kind of this UxOption.

        Kind defines type of option  # noqa: E501

        :param kind: The kind of this UxOption.  # noqa: E501
        :type: str
        """

        self._kind = kind

    @property
    def last_modified_at(self):
        """Gets the last_modified_at of this UxOption.  # noqa: E501

        LastModifiedAt is the time that this object was last modified.  # noqa: E501

        :return: The last_modified_at of this UxOption.  # noqa: E501
        :rtype: datetime
        """
        return self._last_modified_at

    @last_modified_at.setter
    def last_modified_at(self, last_modified_at):
        """Sets the last_modified_at of this UxOption.

        LastModifiedAt is the time that this object was last modified.  # noqa: E501

        :param last_modified_at: The last_modified_at of this UxOption.  # noqa: E501
        :type: datetime
        """

        self._last_modified_at = last_modified_at

    @property
    def last_modified_by(self):
        """Gets the last_modified_by of this UxOption.  # noqa: E501

        LastModifiedBy stores the value of the user that last modified this object. NOTE: This value is populated ONLY if the object was modified by a user which means any actions done using machine tokens will not get tracked  # noqa: E501

        :return: The last_modified_by of this UxOption.  # noqa: E501
        :rtype: str
        """
        return self._last_modified_by

    @last_modified_by.setter
    def last_modified_by(self, last_modified_by):
        """Sets the last_modified_by of this UxOption.

        LastModifiedBy stores the value of the user that last modified this object. NOTE: This value is populated ONLY if the object was modified by a user which means any actions done using machine tokens will not get tracked  # noqa: E501

        :param last_modified_by: The last_modified_by of this UxOption.  # noqa: E501
        :type: str
        """

        self._last_modified_by = last_modified_by

    @property
    def meta(self):
        """Gets the meta of this UxOption.  # noqa: E501


        :return: The meta of this UxOption.  # noqa: E501
        :rtype: Meta
        """
        return self._meta

    @meta.setter
    def meta(self, meta):
        """Sets the meta of this UxOption.


        :param meta: The meta of this UxOption.  # noqa: E501
        :type: Meta
        """

        self._meta = meta

    @property
    def params(self):
        """Gets the params of this UxOption.  # noqa: E501

        Params holds the values of parameters on the object.  The field is a key / value store of the parameters. The key is the name of a parameter.  The key is of type string. The value is the value of the parameter.  The type of the value is defined by the parameter object.  If the key doesn't reference a parameter, the type of the object can be anything.  The system will enforce the named parameter's value's type.  Go calls the \"anything\" parameters as \"interface {}\".  Hence, the type of this field is a map[string]interface{}.  # noqa: E501

        :return: The params of this UxOption.  # noqa: E501
        :rtype: dict(str, object)
        """
        return self._params

    @params.setter
    def params(self, params):
        """Sets the params of this UxOption.

        Params holds the values of parameters on the object.  The field is a key / value store of the parameters. The key is the name of a parameter.  The key is of type string. The value is the value of the parameter.  The type of the value is defined by the parameter object.  If the key doesn't reference a parameter, the type of the object can be anything.  The system will enforce the named parameter's value's type.  Go calls the \"anything\" parameters as \"interface {}\".  Hence, the type of this field is a map[string]interface{}.  # noqa: E501

        :param params: The params of this UxOption.  # noqa: E501
        :type: dict(str, object)
        """

        self._params = params

    @property
    def read_only(self):
        """Gets the read_only of this UxOption.  # noqa: E501

        ReadOnly tracks if the store for this object is read-only. This flag is informational, and cannot be changed via the API.  # noqa: E501

        :return: The read_only of this UxOption.  # noqa: E501
        :rtype: bool
        """
        return self._read_only

    @read_only.setter
    def read_only(self, read_only):
        """Sets the read_only of this UxOption.

        ReadOnly tracks if the store for this object is read-only. This flag is informational, and cannot be changed via the API.  # noqa: E501

        :param read_only: The read_only of this UxOption.  # noqa: E501
        :type: bool
        """

        self._read_only = read_only

    @property
    def role(self):
        """Gets the role of this UxOption.  # noqa: E501

        Roles defines if it is a role scoped option  # noqa: E501

        :return: The role of this UxOption.  # noqa: E501
        :rtype: bool
        """
        return self._role

    @role.setter
    def role(self, role):
        """Sets the role of this UxOption.

        Roles defines if it is a role scoped option  # noqa: E501

        :param role: The role of this UxOption.  # noqa: E501
        :type: bool
        """

        self._role = role

    @property
    def user(self):
        """Gets the user of this UxOption.  # noqa: E501

        User defines if it is a user scoped option  # noqa: E501

        :return: The user of this UxOption.  # noqa: E501
        :rtype: bool
        """
        return self._user

    @user.setter
    def user(self, user):
        """Sets the user of this UxOption.

        User defines if it is a user scoped option  # noqa: E501

        :param user: The user of this UxOption.  # noqa: E501
        :type: bool
        """

        self._user = user

    @property
    def validated(self):
        """Gets the validated of this UxOption.  # noqa: E501

        Validated tracks whether or not the model has been validated.  # noqa: E501

        :return: The validated of this UxOption.  # noqa: E501
        :rtype: bool
        """
        return self._validated

    @validated.setter
    def validated(self, validated):
        """Sets the validated of this UxOption.

        Validated tracks whether or not the model has been validated.  # noqa: E501

        :param validated: The validated of this UxOption.  # noqa: E501
        :type: bool
        """

        self._validated = validated

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(UxOption, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, UxOption):
            return False

        return self.to_dict() == other.to_dict()

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        if not isinstance(other, UxOption):
            return True

        return self.to_dict() != other.to_dict()
