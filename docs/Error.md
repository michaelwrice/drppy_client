# Error

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **int** | Code is the HTTP status code that should be used for this Error | [optional] 
**key** | **str** | Key is the unique per-model identifier of the object to which the error refers. It is generally whatever thing.Key() would return. | [optional] 
**messages** | **list[str]** | Messages are any additional messages related to this Error | [optional] 
**model** | **str** | Model is the type of object to which the error refers.  It is generally whatever thing.Prefix() would return. | [optional] 
**type** | **str** | Type is the type of error this is.  There is no set definition for what this us. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


