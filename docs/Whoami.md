# Whoami

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fingerprint** | [**MachineFingerprint**](MachineFingerprint.md) |  | [optional] 
**mac_addrs** | **list[str]** |  | [optional] 
**on_disk_uuid** | **str** |  | [optional] 
**result** | [**WhoamiResult**](WhoamiResult.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


