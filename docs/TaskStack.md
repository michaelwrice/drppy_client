# TaskStack

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**current_job** | **str** |  | [optional] 
**current_task** | **int** |  | [optional] 
**task_list** | **list[str]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


