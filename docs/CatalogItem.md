# CatalogItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**actual_version** | **str** |  | [optional] 
**available** | **bool** | Available tracks whether or not the model passed validation. | [optional] 
**content_type** | **str** |  | [optional] 
**endpoint** | **str** | Endpoint tracks the owner of the object among DRP endpoints | [optional] 
**errors** | **list[str]** | If there are any errors in the validation process, they will be available here. | [optional] 
**hot_fix** | **bool** |  | [optional] 
**id** | **str** | Id is the unique ID for this catalog item. | [optional] 
**nojq_source** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**read_only** | **bool** | ReadOnly tracks if the store for this object is read-only. This flag is informational, and cannot be changed via the API. | [optional] 
**shasum256** | **dict(str, str)** |  | [optional] 
**source** | **str** |  | [optional] 
**tip** | **bool** |  | [optional] 
**type** | **str** | Type is the type of catalog item this is. | [optional] 
**validated** | **bool** | Validated tracks whether or not the model has been validated. | [optional] 
**version** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


