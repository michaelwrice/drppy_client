# coding: utf-8

"""
    DigitalRebar Provision Server

    An RestFUL API-driven Provisioner and DHCP server  # noqa: E501

    OpenAPI spec version: 4.0.0
    Contact: greg@rackn.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import unittest

import drppy_client
from drppy_client.api.tenants_api import TenantsApi  # noqa: E501
from drppy_client.rest import ApiException


class TestTenantsApi(unittest.TestCase):
    """TenantsApi unit test stubs"""

    def setUp(self):
        self.api = drppy_client.api.tenants_api.TenantsApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_create_tenant(self):
        """Test case for create_tenant

        Create a Tenant  # noqa: E501
        """
        pass

    def test_delete_tenant(self):
        """Test case for delete_tenant

        Delete a Tenant  # noqa: E501
        """
        pass

    def test_get_tenant(self):
        """Test case for get_tenant

        Get a Tenant  # noqa: E501
        """
        pass

    def test_get_tenant_action(self):
        """Test case for get_tenant_action

        List specific action for a tenant Tenant  # noqa: E501
        """
        pass

    def test_get_tenant_actions(self):
        """Test case for get_tenant_actions

        List tenant actions Tenant  # noqa: E501
        """
        pass

    def test_head_tenant(self):
        """Test case for head_tenant

        See if a Tenant exists  # noqa: E501
        """
        pass

    def test_list_stats_tenants(self):
        """Test case for list_stats_tenants

        Stats of the List Tenants filtered by some parameters.  # noqa: E501
        """
        pass

    def test_list_tenants(self):
        """Test case for list_tenants

        Lists Tenants filtered by some parameters.  # noqa: E501
        """
        pass

    def test_patch_tenant(self):
        """Test case for patch_tenant

        Patch a Tenant  # noqa: E501
        """
        pass

    def test_post_tenant_action(self):
        """Test case for post_tenant_action

        Call an action on the node.  # noqa: E501
        """
        pass

    def test_put_tenant(self):
        """Test case for put_tenant

        Put a Tenant  # noqa: E501
        """
        pass


if __name__ == '__main__':
    unittest.main()
