# PoolResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**allocated** | **bool** |  | [optional] 
**name** | **str** |  | [optional] 
**status** | [**PoolStatus**](PoolStatus.md) |  | [optional] 
**uuid** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


