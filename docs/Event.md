# Event

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**action** | **str** | Action - what happened | [optional] 
**key** | **str** | Key - the id of the object | [optional] 
**object** | **object** | Object - the data of the object. | [optional] 
**original** | **object** | Original - the data of the object before the operation (update and save only) | [optional] 
**principal** | **str** | Principal - the user or subsystem that caused the event to be emitted | [optional] 
**time** | **datetime** | Time of the event. | [optional] 
**type** | **str** | Type - object type | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


