# LicenseBundle

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contact** | **str** |  | [optional] 
**contact_email** | **str** |  | [optional] 
**contact_id** | **str** |  | [optional] 
**endpoints** | **list[str]** |  | [optional] 
**generation_version** | **str** |  | [optional] 
**grantor** | **str** |  | [optional] 
**grantor_email** | **str** |  | [optional] 
**licenses** | [**list[License]**](License.md) |  | [optional] 
**max_drp_version** | **str** | MaxDrpVersion is the highest major/minor version to allow. For example, v4.3 would mean any v4.3.* release or previous. | [optional] 
**owner** | **str** |  | [optional] 
**owner_email** | **str** |  | [optional] 
**owner_id** | **str** |  | [optional] 
**version** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


