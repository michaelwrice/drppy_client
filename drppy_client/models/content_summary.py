# coding: utf-8

"""
    DigitalRebar Provision Server

    # An RestFUL API-driven Provisioner and DHCP server  # noqa: E501

    OpenAPI spec version: 4.14.0
    Contact: support@rackn.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six

from drppy_client.configuration import Configuration


class ContentSummary(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'counts': 'dict(str, int)',
        'warnings': 'list[str]',
        'meta': 'ContentMetaData'
    }

    attribute_map = {
        'counts': 'Counts',
        'warnings': 'Warnings',
        'meta': 'meta'
    }

    def __init__(self, counts=None, warnings=None, meta=None, _configuration=None):  # noqa: E501
        """ContentSummary - a model defined in Swagger"""  # noqa: E501
        if _configuration is None:
            _configuration = Configuration()
        self._configuration = _configuration

        self._counts = None
        self._warnings = None
        self._meta = None
        self.discriminator = None

        if counts is not None:
            self.counts = counts
        if warnings is not None:
            self.warnings = warnings
        if meta is not None:
            self.meta = meta

    @property
    def counts(self):
        """Gets the counts of this ContentSummary.  # noqa: E501


        :return: The counts of this ContentSummary.  # noqa: E501
        :rtype: dict(str, int)
        """
        return self._counts

    @counts.setter
    def counts(self, counts):
        """Sets the counts of this ContentSummary.


        :param counts: The counts of this ContentSummary.  # noqa: E501
        :type: dict(str, int)
        """

        self._counts = counts

    @property
    def warnings(self):
        """Gets the warnings of this ContentSummary.  # noqa: E501


        :return: The warnings of this ContentSummary.  # noqa: E501
        :rtype: list[str]
        """
        return self._warnings

    @warnings.setter
    def warnings(self, warnings):
        """Sets the warnings of this ContentSummary.


        :param warnings: The warnings of this ContentSummary.  # noqa: E501
        :type: list[str]
        """

        self._warnings = warnings

    @property
    def meta(self):
        """Gets the meta of this ContentSummary.  # noqa: E501


        :return: The meta of this ContentSummary.  # noqa: E501
        :rtype: ContentMetaData
        """
        return self._meta

    @meta.setter
    def meta(self, meta):
        """Sets the meta of this ContentSummary.


        :param meta: The meta of this ContentSummary.  # noqa: E501
        :type: ContentMetaData
        """

        self._meta = meta

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(ContentSummary, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, ContentSummary):
            return False

        return self.to_dict() == other.to_dict()

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        if not isinstance(other, ContentSummary):
            return True

        return self.to_dict() != other.to_dict()
