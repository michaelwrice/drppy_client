# License

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**active** | **bool** |  | [optional] 
**contract_termination_date** | **datetime** |  | [optional] 
**data** | **object** |  | [optional] 
**hard_expire_date** | **datetime** |  | [optional] 
**long_license** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**purchase_date** | **datetime** |  | [optional] 
**short_license** | **str** |  | [optional] 
**soft_expire_date** | **datetime** |  | [optional] 
**start_date** | **datetime** |  | [optional] 
**version** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


