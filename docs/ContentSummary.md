# ContentSummary

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**counts** | **dict(str, int)** |  | [optional] 
**warnings** | **list[str]** |  | [optional] 
**meta** | [**ContentMetaData**](ContentMetaData.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


