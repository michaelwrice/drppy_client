# MenuItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**color** | **str** |  | [optional] 
**filter** | **str** |  | [optional] 
**group_by** | **list[str]** |  | [optional] 
**has_flag** | **str** |  | [optional] 
**has_object** | **str** |  | [optional] 
**icon** | **str** |  | [optional] 
**id** | **str** |  | [optional] 
**overwrite** | **str** |  | [optional] 
**title** | **str** |  | [optional] 
**to** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


