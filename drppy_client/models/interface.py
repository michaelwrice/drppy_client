# coding: utf-8

"""
    DigitalRebar Provision Server

    # An RestFUL API-driven Provisioner and DHCP server  # noqa: E501

    OpenAPI spec version: 4.14.0
    Contact: support@rackn.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six

from drppy_client.configuration import Configuration


class Interface(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'active_address': 'str',
        'addresses': 'list[str]',
        'created_at': 'datetime',
        'created_by': 'str',
        'dns_domain': 'str',
        'dns_servers': 'list[str]',
        'gateway': 'str',
        'index': 'int',
        'last_modified_at': 'datetime',
        'last_modified_by': 'str',
        'meta': 'Meta',
        'name': 'str',
        'read_only': 'bool'
    }

    attribute_map = {
        'active_address': 'ActiveAddress',
        'addresses': 'Addresses',
        'created_at': 'CreatedAt',
        'created_by': 'CreatedBy',
        'dns_domain': 'DnsDomain',
        'dns_servers': 'DnsServers',
        'gateway': 'Gateway',
        'index': 'Index',
        'last_modified_at': 'LastModifiedAt',
        'last_modified_by': 'LastModifiedBy',
        'meta': 'Meta',
        'name': 'Name',
        'read_only': 'ReadOnly'
    }

    def __init__(self, active_address=None, addresses=None, created_at=None, created_by=None, dns_domain=None, dns_servers=None, gateway=None, index=None, last_modified_at=None, last_modified_by=None, meta=None, name=None, read_only=None, _configuration=None):  # noqa: E501
        """Interface - a model defined in Swagger"""  # noqa: E501
        if _configuration is None:
            _configuration = Configuration()
        self._configuration = _configuration

        self._active_address = None
        self._addresses = None
        self._created_at = None
        self._created_by = None
        self._dns_domain = None
        self._dns_servers = None
        self._gateway = None
        self._index = None
        self._last_modified_at = None
        self._last_modified_by = None
        self._meta = None
        self._name = None
        self._read_only = None
        self.discriminator = None

        if active_address is not None:
            self.active_address = active_address
        self.addresses = addresses
        if created_at is not None:
            self.created_at = created_at
        if created_by is not None:
            self.created_by = created_by
        if dns_domain is not None:
            self.dns_domain = dns_domain
        if dns_servers is not None:
            self.dns_servers = dns_servers
        if gateway is not None:
            self.gateway = gateway
        if index is not None:
            self.index = index
        if last_modified_at is not None:
            self.last_modified_at = last_modified_at
        if last_modified_by is not None:
            self.last_modified_by = last_modified_by
        if meta is not None:
            self.meta = meta
        self.name = name
        if read_only is not None:
            self.read_only = read_only

    @property
    def active_address(self):
        """Gets the active_address of this Interface.  # noqa: E501

        ActiveAddress is our best guess at the address that should be used for \"normal\" incoming traffic on this interface.  # noqa: E501

        :return: The active_address of this Interface.  # noqa: E501
        :rtype: str
        """
        return self._active_address

    @active_address.setter
    def active_address(self, active_address):
        """Sets the active_address of this Interface.

        ActiveAddress is our best guess at the address that should be used for \"normal\" incoming traffic on this interface.  # noqa: E501

        :param active_address: The active_address of this Interface.  # noqa: E501
        :type: str
        """

        self._active_address = active_address

    @property
    def addresses(self):
        """Gets the addresses of this Interface.  # noqa: E501

        Addresses contains the IPv4 and IPv6 addresses bound to this interface in no particular order.  # noqa: E501

        :return: The addresses of this Interface.  # noqa: E501
        :rtype: list[str]
        """
        return self._addresses

    @addresses.setter
    def addresses(self, addresses):
        """Sets the addresses of this Interface.

        Addresses contains the IPv4 and IPv6 addresses bound to this interface in no particular order.  # noqa: E501

        :param addresses: The addresses of this Interface.  # noqa: E501
        :type: list[str]
        """
        if self._configuration.client_side_validation and addresses is None:
            raise ValueError("Invalid value for `addresses`, must not be `None`")  # noqa: E501

        self._addresses = addresses

    @property
    def created_at(self):
        """Gets the created_at of this Interface.  # noqa: E501

        CreatedAt is the time that this object was created.  # noqa: E501

        :return: The created_at of this Interface.  # noqa: E501
        :rtype: datetime
        """
        return self._created_at

    @created_at.setter
    def created_at(self, created_at):
        """Sets the created_at of this Interface.

        CreatedAt is the time that this object was created.  # noqa: E501

        :param created_at: The created_at of this Interface.  # noqa: E501
        :type: datetime
        """

        self._created_at = created_at

    @property
    def created_by(self):
        """Gets the created_by of this Interface.  # noqa: E501

        CreatedBy stores the value of the user that created this object. Note: This value is stored ONLY if the object was created by a user which means that `currentUserName` needs to be populated in the authBlob  # noqa: E501

        :return: The created_by of this Interface.  # noqa: E501
        :rtype: str
        """
        return self._created_by

    @created_by.setter
    def created_by(self, created_by):
        """Sets the created_by of this Interface.

        CreatedBy stores the value of the user that created this object. Note: This value is stored ONLY if the object was created by a user which means that `currentUserName` needs to be populated in the authBlob  # noqa: E501

        :param created_by: The created_by of this Interface.  # noqa: E501
        :type: str
        """

        self._created_by = created_by

    @property
    def dns_domain(self):
        """Gets the dns_domain of this Interface.  # noqa: E501

        DnsDomain is the domain that this system appears to be in on this interface.  # noqa: E501

        :return: The dns_domain of this Interface.  # noqa: E501
        :rtype: str
        """
        return self._dns_domain

    @dns_domain.setter
    def dns_domain(self, dns_domain):
        """Sets the dns_domain of this Interface.

        DnsDomain is the domain that this system appears to be in on this interface.  # noqa: E501

        :param dns_domain: The dns_domain of this Interface.  # noqa: E501
        :type: str
        """

        self._dns_domain = dns_domain

    @property
    def dns_servers(self):
        """Gets the dns_servers of this Interface.  # noqa: E501

        DnsServers is a list of DNS server that hsould be used when resolving addresses via this interface.  # noqa: E501

        :return: The dns_servers of this Interface.  # noqa: E501
        :rtype: list[str]
        """
        return self._dns_servers

    @dns_servers.setter
    def dns_servers(self, dns_servers):
        """Sets the dns_servers of this Interface.

        DnsServers is a list of DNS server that hsould be used when resolving addresses via this interface.  # noqa: E501

        :param dns_servers: The dns_servers of this Interface.  # noqa: E501
        :type: list[str]
        """

        self._dns_servers = dns_servers

    @property
    def gateway(self):
        """Gets the gateway of this Interface.  # noqa: E501

        Gateway is our best guess about the IP address that traffic forwarded through this interface should be sent to.  # noqa: E501

        :return: The gateway of this Interface.  # noqa: E501
        :rtype: str
        """
        return self._gateway

    @gateway.setter
    def gateway(self, gateway):
        """Sets the gateway of this Interface.

        Gateway is our best guess about the IP address that traffic forwarded through this interface should be sent to.  # noqa: E501

        :param gateway: The gateway of this Interface.  # noqa: E501
        :type: str
        """

        self._gateway = gateway

    @property
    def index(self):
        """Gets the index of this Interface.  # noqa: E501

        Index of the interface.  This is OS specific.  # noqa: E501

        :return: The index of this Interface.  # noqa: E501
        :rtype: int
        """
        return self._index

    @index.setter
    def index(self, index):
        """Sets the index of this Interface.

        Index of the interface.  This is OS specific.  # noqa: E501

        :param index: The index of this Interface.  # noqa: E501
        :type: int
        """

        self._index = index

    @property
    def last_modified_at(self):
        """Gets the last_modified_at of this Interface.  # noqa: E501

        LastModifiedAt is the time that this object was last modified.  # noqa: E501

        :return: The last_modified_at of this Interface.  # noqa: E501
        :rtype: datetime
        """
        return self._last_modified_at

    @last_modified_at.setter
    def last_modified_at(self, last_modified_at):
        """Sets the last_modified_at of this Interface.

        LastModifiedAt is the time that this object was last modified.  # noqa: E501

        :param last_modified_at: The last_modified_at of this Interface.  # noqa: E501
        :type: datetime
        """

        self._last_modified_at = last_modified_at

    @property
    def last_modified_by(self):
        """Gets the last_modified_by of this Interface.  # noqa: E501

        LastModifiedBy stores the value of the user that last modified this object. NOTE: This value is populated ONLY if the object was modified by a user which means any actions done using machine tokens will not get tracked  # noqa: E501

        :return: The last_modified_by of this Interface.  # noqa: E501
        :rtype: str
        """
        return self._last_modified_by

    @last_modified_by.setter
    def last_modified_by(self, last_modified_by):
        """Sets the last_modified_by of this Interface.

        LastModifiedBy stores the value of the user that last modified this object. NOTE: This value is populated ONLY if the object was modified by a user which means any actions done using machine tokens will not get tracked  # noqa: E501

        :param last_modified_by: The last_modified_by of this Interface.  # noqa: E501
        :type: str
        """

        self._last_modified_by = last_modified_by

    @property
    def meta(self):
        """Gets the meta of this Interface.  # noqa: E501


        :return: The meta of this Interface.  # noqa: E501
        :rtype: Meta
        """
        return self._meta

    @meta.setter
    def meta(self, meta):
        """Sets the meta of this Interface.


        :param meta: The meta of this Interface.  # noqa: E501
        :type: Meta
        """

        self._meta = meta

    @property
    def name(self):
        """Gets the name of this Interface.  # noqa: E501

        Name of the interface  # noqa: E501

        :return: The name of this Interface.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this Interface.

        Name of the interface  # noqa: E501

        :param name: The name of this Interface.  # noqa: E501
        :type: str
        """
        if self._configuration.client_side_validation and name is None:
            raise ValueError("Invalid value for `name`, must not be `None`")  # noqa: E501

        self._name = name

    @property
    def read_only(self):
        """Gets the read_only of this Interface.  # noqa: E501

        ReadOnly tracks if the store for this object is read-only. This flag is informational, and cannot be changed via the API.  # noqa: E501

        :return: The read_only of this Interface.  # noqa: E501
        :rtype: bool
        """
        return self._read_only

    @read_only.setter
    def read_only(self, read_only):
        """Sets the read_only of this Interface.

        ReadOnly tracks if the store for this object is read-only. This flag is informational, and cannot be changed via the API.  # noqa: E501

        :param read_only: The read_only of this Interface.  # noqa: E501
        :type: bool
        """

        self._read_only = read_only

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(Interface, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, Interface):
            return False

        return self.to_dict() == other.to_dict()

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        if not isinstance(other, Interface):
            return True

        return self.to_dict() != other.to_dict()
