# coding: utf-8

"""
    DigitalRebar Provision Server

    An RestFUL API-driven Provisioner and DHCP server  # noqa: E501

    OpenAPI spec version: 4.0.0
    Contact: greg@rackn.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import unittest

import drppy_client
from drppy_client.models.blob_info import BlobInfo  # noqa: E501
from drppy_client.rest import ApiException


class TestBlobInfo(unittest.TestCase):
    """BlobInfo unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testBlobInfo(self):
        """Test BlobInfo"""
        # FIXME: construct object with mandatory attributes with example values
        # model = drppy_client.models.blob_info.BlobInfo()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
