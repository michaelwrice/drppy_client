# coding: utf-8

"""
    DigitalRebar Provision Server

    # An RestFUL API-driven Provisioner and DHCP server  # noqa: E501

    OpenAPI spec version: 4.14.0
    Contact: support@rackn.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six

from drppy_client.configuration import Configuration


class Endpoint(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'action_error_count': 'int',
        'actions': 'list[ElementAction]',
        'apply': 'bool',
        'arch': 'str',
        'available': 'bool',
        'bundle': 'str',
        'components': 'list[Element]',
        'connection_status': 'str',
        'created_at': 'datetime',
        'created_by': 'str',
        'drpux_version': 'str',
        'drp_version': 'str',
        'description': 'str',
        'documentation': 'str',
        'endpoint': 'str',
        'errors': 'list[str]',
        '_global': 'dict(str, object)',
        'ha_id': 'str',
        'id': 'str',
        'last_modified_at': 'datetime',
        'last_modified_by': 'str',
        'meta': 'Meta',
        'os': 'str',
        'params': 'dict(str, object)',
        'plugins': 'list[Plugin]',
        'prefs': 'dict(str, str)',
        'read_only': 'bool',
        'validated': 'bool',
        'version_set': 'str',
        'version_sets': 'list[str]'
    }

    attribute_map = {
        'action_error_count': 'ActionErrorCount',
        'actions': 'Actions',
        'apply': 'Apply',
        'arch': 'Arch',
        'available': 'Available',
        'bundle': 'Bundle',
        'components': 'Components',
        'connection_status': 'ConnectionStatus',
        'created_at': 'CreatedAt',
        'created_by': 'CreatedBy',
        'drpux_version': 'DRPUXVersion',
        'drp_version': 'DRPVersion',
        'description': 'Description',
        'documentation': 'Documentation',
        'endpoint': 'Endpoint',
        'errors': 'Errors',
        '_global': 'Global',
        'ha_id': 'HaId',
        'id': 'Id',
        'last_modified_at': 'LastModifiedAt',
        'last_modified_by': 'LastModifiedBy',
        'meta': 'Meta',
        'os': 'Os',
        'params': 'Params',
        'plugins': 'Plugins',
        'prefs': 'Prefs',
        'read_only': 'ReadOnly',
        'validated': 'Validated',
        'version_set': 'VersionSet',
        'version_sets': 'VersionSets'
    }

    def __init__(self, action_error_count=None, actions=None, apply=None, arch=None, available=None, bundle=None, components=None, connection_status=None, created_at=None, created_by=None, drpux_version=None, drp_version=None, description=None, documentation=None, endpoint=None, errors=None, _global=None, ha_id=None, id=None, last_modified_at=None, last_modified_by=None, meta=None, os=None, params=None, plugins=None, prefs=None, read_only=None, validated=None, version_set=None, version_sets=None, _configuration=None):  # noqa: E501
        """Endpoint - a model defined in Swagger"""  # noqa: E501
        if _configuration is None:
            _configuration = Configuration()
        self._configuration = _configuration

        self._action_error_count = None
        self._actions = None
        self._apply = None
        self._arch = None
        self._available = None
        self._bundle = None
        self._components = None
        self._connection_status = None
        self._created_at = None
        self._created_by = None
        self._drpux_version = None
        self._drp_version = None
        self._description = None
        self._documentation = None
        self._endpoint = None
        self._errors = None
        self.__global = None
        self._ha_id = None
        self._id = None
        self._last_modified_at = None
        self._last_modified_by = None
        self._meta = None
        self._os = None
        self._params = None
        self._plugins = None
        self._prefs = None
        self._read_only = None
        self._validated = None
        self._version_set = None
        self._version_sets = None
        self.discriminator = None

        if action_error_count is not None:
            self.action_error_count = action_error_count
        if actions is not None:
            self.actions = actions
        if apply is not None:
            self.apply = apply
        if arch is not None:
            self.arch = arch
        if available is not None:
            self.available = available
        if bundle is not None:
            self.bundle = bundle
        if components is not None:
            self.components = components
        if connection_status is not None:
            self.connection_status = connection_status
        if created_at is not None:
            self.created_at = created_at
        if created_by is not None:
            self.created_by = created_by
        if drpux_version is not None:
            self.drpux_version = drpux_version
        if drp_version is not None:
            self.drp_version = drp_version
        if description is not None:
            self.description = description
        if documentation is not None:
            self.documentation = documentation
        if endpoint is not None:
            self.endpoint = endpoint
        if errors is not None:
            self.errors = errors
        if _global is not None:
            self._global = _global
        if ha_id is not None:
            self.ha_id = ha_id
        if id is not None:
            self.id = id
        if last_modified_at is not None:
            self.last_modified_at = last_modified_at
        if last_modified_by is not None:
            self.last_modified_by = last_modified_by
        if meta is not None:
            self.meta = meta
        if os is not None:
            self.os = os
        if params is not None:
            self.params = params
        if plugins is not None:
            self.plugins = plugins
        if prefs is not None:
            self.prefs = prefs
        if read_only is not None:
            self.read_only = read_only
        if validated is not None:
            self.validated = validated
        if version_set is not None:
            self.version_set = version_set
        if version_sets is not None:
            self.version_sets = version_sets

    @property
    def action_error_count(self):
        """Gets the action_error_count of this Endpoint.  # noqa: E501

        ActionErrorCount is the number of failed actions in the action list If the whole list is tried and no progress is made the apply will stop.  # noqa: E501

        :return: The action_error_count of this Endpoint.  # noqa: E501
        :rtype: int
        """
        return self._action_error_count

    @action_error_count.setter
    def action_error_count(self, action_error_count):
        """Sets the action_error_count of this Endpoint.

        ActionErrorCount is the number of failed actions in the action list If the whole list is tried and no progress is made the apply will stop.  # noqa: E501

        :param action_error_count: The action_error_count of this Endpoint.  # noqa: E501
        :type: int
        """

        self._action_error_count = action_error_count

    @property
    def actions(self):
        """Gets the actions of this Endpoint.  # noqa: E501

        Actions is the list of actions to take to make the endpoint match the version sets on in the endpoint object.  # noqa: E501

        :return: The actions of this Endpoint.  # noqa: E501
        :rtype: list[ElementAction]
        """
        return self._actions

    @actions.setter
    def actions(self, actions):
        """Sets the actions of this Endpoint.

        Actions is the list of actions to take to make the endpoint match the version sets on in the endpoint object.  # noqa: E501

        :param actions: The actions of this Endpoint.  # noqa: E501
        :type: list[ElementAction]
        """

        self._actions = actions

    @property
    def apply(self):
        """Gets the apply of this Endpoint.  # noqa: E501

        Apply toggles whether the manager should update the endpoint.  # noqa: E501

        :return: The apply of this Endpoint.  # noqa: E501
        :rtype: bool
        """
        return self._apply

    @apply.setter
    def apply(self, apply):
        """Sets the apply of this Endpoint.

        Apply toggles whether the manager should update the endpoint.  # noqa: E501

        :param apply: The apply of this Endpoint.  # noqa: E501
        :type: bool
        """

        self._apply = apply

    @property
    def arch(self):
        """Gets the arch of this Endpoint.  # noqa: E501

        Arch is the arch of the endpoint - Golang arch format.  # noqa: E501

        :return: The arch of this Endpoint.  # noqa: E501
        :rtype: str
        """
        return self._arch

    @arch.setter
    def arch(self, arch):
        """Sets the arch of this Endpoint.

        Arch is the arch of the endpoint - Golang arch format.  # noqa: E501

        :param arch: The arch of this Endpoint.  # noqa: E501
        :type: str
        """

        self._arch = arch

    @property
    def available(self):
        """Gets the available of this Endpoint.  # noqa: E501

        Available tracks whether or not the model passed validation.  # noqa: E501

        :return: The available of this Endpoint.  # noqa: E501
        :rtype: bool
        """
        return self._available

    @available.setter
    def available(self, available):
        """Sets the available of this Endpoint.

        Available tracks whether or not the model passed validation.  # noqa: E501

        :param available: The available of this Endpoint.  # noqa: E501
        :type: bool
        """

        self._available = available

    @property
    def bundle(self):
        """Gets the bundle of this Endpoint.  # noqa: E501

        Bundle tracks the name of the store containing this object. This field is read-only, and cannot be changed via the API.  # noqa: E501

        :return: The bundle of this Endpoint.  # noqa: E501
        :rtype: str
        """
        return self._bundle

    @bundle.setter
    def bundle(self, bundle):
        """Sets the bundle of this Endpoint.

        Bundle tracks the name of the store containing this object. This field is read-only, and cannot be changed via the API.  # noqa: E501

        :param bundle: The bundle of this Endpoint.  # noqa: E501
        :type: str
        """

        self._bundle = bundle

    @property
    def components(self):
        """Gets the components of this Endpoint.  # noqa: E501

        Components is the list of ContentPackages and PluginProviders installed and their versions  # noqa: E501

        :return: The components of this Endpoint.  # noqa: E501
        :rtype: list[Element]
        """
        return self._components

    @components.setter
    def components(self, components):
        """Sets the components of this Endpoint.

        Components is the list of ContentPackages and PluginProviders installed and their versions  # noqa: E501

        :param components: The components of this Endpoint.  # noqa: E501
        :type: list[Element]
        """

        self._components = components

    @property
    def connection_status(self):
        """Gets the connection_status of this Endpoint.  # noqa: E501

        ConnectionStatus reflects the manager's state of interaction with the endpoint  # noqa: E501

        :return: The connection_status of this Endpoint.  # noqa: E501
        :rtype: str
        """
        return self._connection_status

    @connection_status.setter
    def connection_status(self, connection_status):
        """Sets the connection_status of this Endpoint.

        ConnectionStatus reflects the manager's state of interaction with the endpoint  # noqa: E501

        :param connection_status: The connection_status of this Endpoint.  # noqa: E501
        :type: str
        """

        self._connection_status = connection_status

    @property
    def created_at(self):
        """Gets the created_at of this Endpoint.  # noqa: E501

        CreatedAt is the time that this object was created.  # noqa: E501

        :return: The created_at of this Endpoint.  # noqa: E501
        :rtype: datetime
        """
        return self._created_at

    @created_at.setter
    def created_at(self, created_at):
        """Sets the created_at of this Endpoint.

        CreatedAt is the time that this object was created.  # noqa: E501

        :param created_at: The created_at of this Endpoint.  # noqa: E501
        :type: datetime
        """

        self._created_at = created_at

    @property
    def created_by(self):
        """Gets the created_by of this Endpoint.  # noqa: E501

        CreatedBy stores the value of the user that created this object. Note: This value is stored ONLY if the object was created by a user which means that `currentUserName` needs to be populated in the authBlob  # noqa: E501

        :return: The created_by of this Endpoint.  # noqa: E501
        :rtype: str
        """
        return self._created_by

    @created_by.setter
    def created_by(self, created_by):
        """Sets the created_by of this Endpoint.

        CreatedBy stores the value of the user that created this object. Note: This value is stored ONLY if the object was created by a user which means that `currentUserName` needs to be populated in the authBlob  # noqa: E501

        :param created_by: The created_by of this Endpoint.  # noqa: E501
        :type: str
        """

        self._created_by = created_by

    @property
    def drpux_version(self):
        """Gets the drpux_version of this Endpoint.  # noqa: E501

        DRPUXVersion is the version of the ux installed on the endpoint.  # noqa: E501

        :return: The drpux_version of this Endpoint.  # noqa: E501
        :rtype: str
        """
        return self._drpux_version

    @drpux_version.setter
    def drpux_version(self, drpux_version):
        """Sets the drpux_version of this Endpoint.

        DRPUXVersion is the version of the ux installed on the endpoint.  # noqa: E501

        :param drpux_version: The drpux_version of this Endpoint.  # noqa: E501
        :type: str
        """

        self._drpux_version = drpux_version

    @property
    def drp_version(self):
        """Gets the drp_version of this Endpoint.  # noqa: E501

        DRPVersion is the version of the drp endpoint running.  # noqa: E501

        :return: The drp_version of this Endpoint.  # noqa: E501
        :rtype: str
        """
        return self._drp_version

    @drp_version.setter
    def drp_version(self, drp_version):
        """Sets the drp_version of this Endpoint.

        DRPVersion is the version of the drp endpoint running.  # noqa: E501

        :param drp_version: The drp_version of this Endpoint.  # noqa: E501
        :type: str
        """

        self._drp_version = drp_version

    @property
    def description(self):
        """Gets the description of this Endpoint.  # noqa: E501

        Description is a string for providing a simple description  # noqa: E501

        :return: The description of this Endpoint.  # noqa: E501
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description):
        """Sets the description of this Endpoint.

        Description is a string for providing a simple description  # noqa: E501

        :param description: The description of this Endpoint.  # noqa: E501
        :type: str
        """

        self._description = description

    @property
    def documentation(self):
        """Gets the documentation of this Endpoint.  # noqa: E501

        Documentation is a string for providing additional in depth information.  # noqa: E501

        :return: The documentation of this Endpoint.  # noqa: E501
        :rtype: str
        """
        return self._documentation

    @documentation.setter
    def documentation(self, documentation):
        """Sets the documentation of this Endpoint.

        Documentation is a string for providing additional in depth information.  # noqa: E501

        :param documentation: The documentation of this Endpoint.  # noqa: E501
        :type: str
        """

        self._documentation = documentation

    @property
    def endpoint(self):
        """Gets the endpoint of this Endpoint.  # noqa: E501

        Endpoint tracks the owner of the object among DRP endpoints  # noqa: E501

        :return: The endpoint of this Endpoint.  # noqa: E501
        :rtype: str
        """
        return self._endpoint

    @endpoint.setter
    def endpoint(self, endpoint):
        """Sets the endpoint of this Endpoint.

        Endpoint tracks the owner of the object among DRP endpoints  # noqa: E501

        :param endpoint: The endpoint of this Endpoint.  # noqa: E501
        :type: str
        """

        self._endpoint = endpoint

    @property
    def errors(self):
        """Gets the errors of this Endpoint.  # noqa: E501

        If there are any errors in the validation process, they will be available here.  # noqa: E501

        :return: The errors of this Endpoint.  # noqa: E501
        :rtype: list[str]
        """
        return self._errors

    @errors.setter
    def errors(self, errors):
        """Sets the errors of this Endpoint.

        If there are any errors in the validation process, they will be available here.  # noqa: E501

        :param errors: The errors of this Endpoint.  # noqa: E501
        :type: list[str]
        """

        self._errors = errors

    @property
    def _global(self):
        """Gets the _global of this Endpoint.  # noqa: E501

        Global is the Parameters of the global profile.  # noqa: E501

        :return: The _global of this Endpoint.  # noqa: E501
        :rtype: dict(str, object)
        """
        return self.__global

    @_global.setter
    def _global(self, _global):
        """Sets the _global of this Endpoint.

        Global is the Parameters of the global profile.  # noqa: E501

        :param _global: The _global of this Endpoint.  # noqa: E501
        :type: dict(str, object)
        """

        self.__global = _global

    @property
    def ha_id(self):
        """Gets the ha_id of this Endpoint.  # noqa: E501

        HaId is the HaId of the endpoint  # noqa: E501

        :return: The ha_id of this Endpoint.  # noqa: E501
        :rtype: str
        """
        return self._ha_id

    @ha_id.setter
    def ha_id(self, ha_id):
        """Sets the ha_id of this Endpoint.

        HaId is the HaId of the endpoint  # noqa: E501

        :param ha_id: The ha_id of this Endpoint.  # noqa: E501
        :type: str
        """

        self._ha_id = ha_id

    @property
    def id(self):
        """Gets the id of this Endpoint.  # noqa: E501

        Id is the name of the DRP endpoint this should match the HA pair's ID or the DRP ID of a single node.  # noqa: E501

        :return: The id of this Endpoint.  # noqa: E501
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this Endpoint.

        Id is the name of the DRP endpoint this should match the HA pair's ID or the DRP ID of a single node.  # noqa: E501

        :param id: The id of this Endpoint.  # noqa: E501
        :type: str
        """

        self._id = id

    @property
    def last_modified_at(self):
        """Gets the last_modified_at of this Endpoint.  # noqa: E501

        LastModifiedAt is the time that this object was last modified.  # noqa: E501

        :return: The last_modified_at of this Endpoint.  # noqa: E501
        :rtype: datetime
        """
        return self._last_modified_at

    @last_modified_at.setter
    def last_modified_at(self, last_modified_at):
        """Sets the last_modified_at of this Endpoint.

        LastModifiedAt is the time that this object was last modified.  # noqa: E501

        :param last_modified_at: The last_modified_at of this Endpoint.  # noqa: E501
        :type: datetime
        """

        self._last_modified_at = last_modified_at

    @property
    def last_modified_by(self):
        """Gets the last_modified_by of this Endpoint.  # noqa: E501

        LastModifiedBy stores the value of the user that last modified this object. NOTE: This value is populated ONLY if the object was modified by a user which means any actions done using machine tokens will not get tracked  # noqa: E501

        :return: The last_modified_by of this Endpoint.  # noqa: E501
        :rtype: str
        """
        return self._last_modified_by

    @last_modified_by.setter
    def last_modified_by(self, last_modified_by):
        """Sets the last_modified_by of this Endpoint.

        LastModifiedBy stores the value of the user that last modified this object. NOTE: This value is populated ONLY if the object was modified by a user which means any actions done using machine tokens will not get tracked  # noqa: E501

        :param last_modified_by: The last_modified_by of this Endpoint.  # noqa: E501
        :type: str
        """

        self._last_modified_by = last_modified_by

    @property
    def meta(self):
        """Gets the meta of this Endpoint.  # noqa: E501


        :return: The meta of this Endpoint.  # noqa: E501
        :rtype: Meta
        """
        return self._meta

    @meta.setter
    def meta(self, meta):
        """Sets the meta of this Endpoint.


        :param meta: The meta of this Endpoint.  # noqa: E501
        :type: Meta
        """

        self._meta = meta

    @property
    def os(self):
        """Gets the os of this Endpoint.  # noqa: E501

        Os is the os of the endpoint - Golang os format.  # noqa: E501

        :return: The os of this Endpoint.  # noqa: E501
        :rtype: str
        """
        return self._os

    @os.setter
    def os(self, os):
        """Sets the os of this Endpoint.

        Os is the os of the endpoint - Golang os format.  # noqa: E501

        :param os: The os of this Endpoint.  # noqa: E501
        :type: str
        """

        self._os = os

    @property
    def params(self):
        """Gets the params of this Endpoint.  # noqa: E501

        Params holds the values of parameters on the object.  The field is a key / value store of the parameters. The key is the name of a parameter.  The key is of type string. The value is the value of the parameter.  The type of the value is defined by the parameter object.  If the key doesn't reference a parameter, the type of the object can be anything.  The system will enforce the named parameter's value's type.  Go calls the \"anything\" parameters as \"interface {}\".  Hence, the type of this field is a map[string]interface{}.  # noqa: E501

        :return: The params of this Endpoint.  # noqa: E501
        :rtype: dict(str, object)
        """
        return self._params

    @params.setter
    def params(self, params):
        """Sets the params of this Endpoint.

        Params holds the values of parameters on the object.  The field is a key / value store of the parameters. The key is the name of a parameter.  The key is of type string. The value is the value of the parameter.  The type of the value is defined by the parameter object.  If the key doesn't reference a parameter, the type of the object can be anything.  The system will enforce the named parameter's value's type.  Go calls the \"anything\" parameters as \"interface {}\".  Hence, the type of this field is a map[string]interface{}.  # noqa: E501

        :param params: The params of this Endpoint.  # noqa: E501
        :type: dict(str, object)
        """

        self._params = params

    @property
    def plugins(self):
        """Gets the plugins of this Endpoint.  # noqa: E501

        Plugins is the list of Plugins configured on the endpoint.  # noqa: E501

        :return: The plugins of this Endpoint.  # noqa: E501
        :rtype: list[Plugin]
        """
        return self._plugins

    @plugins.setter
    def plugins(self, plugins):
        """Sets the plugins of this Endpoint.

        Plugins is the list of Plugins configured on the endpoint.  # noqa: E501

        :param plugins: The plugins of this Endpoint.  # noqa: E501
        :type: list[Plugin]
        """

        self._plugins = plugins

    @property
    def prefs(self):
        """Gets the prefs of this Endpoint.  # noqa: E501

        Prefs is the value of all the prefs on the endpoint.  # noqa: E501

        :return: The prefs of this Endpoint.  # noqa: E501
        :rtype: dict(str, str)
        """
        return self._prefs

    @prefs.setter
    def prefs(self, prefs):
        """Sets the prefs of this Endpoint.

        Prefs is the value of all the prefs on the endpoint.  # noqa: E501

        :param prefs: The prefs of this Endpoint.  # noqa: E501
        :type: dict(str, str)
        """

        self._prefs = prefs

    @property
    def read_only(self):
        """Gets the read_only of this Endpoint.  # noqa: E501

        ReadOnly tracks if the store for this object is read-only. This flag is informational, and cannot be changed via the API.  # noqa: E501

        :return: The read_only of this Endpoint.  # noqa: E501
        :rtype: bool
        """
        return self._read_only

    @read_only.setter
    def read_only(self, read_only):
        """Sets the read_only of this Endpoint.

        ReadOnly tracks if the store for this object is read-only. This flag is informational, and cannot be changed via the API.  # noqa: E501

        :param read_only: The read_only of this Endpoint.  # noqa: E501
        :type: bool
        """

        self._read_only = read_only

    @property
    def validated(self):
        """Gets the validated of this Endpoint.  # noqa: E501

        Validated tracks whether or not the model has been validated.  # noqa: E501

        :return: The validated of this Endpoint.  # noqa: E501
        :rtype: bool
        """
        return self._validated

    @validated.setter
    def validated(self, validated):
        """Sets the validated of this Endpoint.

        Validated tracks whether or not the model has been validated.  # noqa: E501

        :param validated: The validated of this Endpoint.  # noqa: E501
        :type: bool
        """

        self._validated = validated

    @property
    def version_set(self):
        """Gets the version_set of this Endpoint.  # noqa: E501

        VersionSet - Deprecated - was a single version set. This should be specified within the VersionSets list  # noqa: E501

        :return: The version_set of this Endpoint.  # noqa: E501
        :rtype: str
        """
        return self._version_set

    @version_set.setter
    def version_set(self, version_set):
        """Sets the version_set of this Endpoint.

        VersionSet - Deprecated - was a single version set. This should be specified within the VersionSets list  # noqa: E501

        :param version_set: The version_set of this Endpoint.  # noqa: E501
        :type: str
        """

        self._version_set = version_set

    @property
    def version_sets(self):
        """Gets the version_sets of this Endpoint.  # noqa: E501

        VersionSets replaces VersionSet - code processes both This is the list of version sets to apply.  These are merged with the first in the list having priority over later elements in the list.  # noqa: E501

        :return: The version_sets of this Endpoint.  # noqa: E501
        :rtype: list[str]
        """
        return self._version_sets

    @version_sets.setter
    def version_sets(self, version_sets):
        """Sets the version_sets of this Endpoint.

        VersionSets replaces VersionSet - code processes both This is the list of version sets to apply.  These are merged with the first in the list having priority over later elements in the list.  # noqa: E501

        :param version_sets: The version_sets of this Endpoint.  # noqa: E501
        :type: list[str]
        """

        self._version_sets = version_sets

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(Endpoint, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, Endpoint):
            return False

        return self.to_dict() == other.to_dict()

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        if not isinstance(other, Endpoint):
            return True

        return self.to_dict() != other.to_dict()
