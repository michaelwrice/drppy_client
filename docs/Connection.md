# Connection

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**create_time** | **datetime** |  | [optional] 
**principal** | **str** |  | [optional] 
**remote_addr** | **str** |  | [optional] 
**type** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


