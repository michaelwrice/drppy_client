# coding: utf-8

"""
    DigitalRebar Provision Server

    An RestFUL API-driven Provisioner and DHCP server  # noqa: E501

    OpenAPI spec version: 4.0.0
    Contact: eng@rackn.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from setuptools import setup, find_packages  # noqa: H301
import os
import glob

NAME = "drppy_client"


def read(fname):
    with open(os.path.join(os.path.dirname(__file__), fname)) as f:
        return f.read()


with open('requirements.txt') as f:
    required = f.read().splitlines()

with open('test-requirements.txt') as f:
    required_for_tests = f.read().splitlines()

setup(
    name=NAME,
    # The version is now handled by setuptools_scm
    description="DigitalRebar Provision Server",
    author_email="eng@rackn.com",
    url="https://gitlab.com/rackn/drppy_client",
    keywords=["Swagger", "DigitalRebar Provision Server"],
    install_requires=required,
    tests_require=required_for_tests,
    packages=find_packages(include=["drppy_client", "drppy_client.*"]),
    data_files=[
        ("share/drppy_client/examples", glob.glob("examples/*")),
        ("share/drppy_client/docs", glob.glob("docs/*")),
    ],
    include_package_data=True,
    exclude_package_data={
        "": ["test/*"],
    },
    long_description=read('README.md'),
    long_description_content_type='text/markdown'
)
