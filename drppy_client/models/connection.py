# coding: utf-8

"""
    DigitalRebar Provision Server

    # An RestFUL API-driven Provisioner and DHCP server  # noqa: E501

    OpenAPI spec version: 4.14.0
    Contact: support@rackn.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six

from drppy_client.configuration import Configuration


class Connection(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'create_time': 'datetime',
        'principal': 'str',
        'remote_addr': 'str',
        'type': 'str'
    }

    attribute_map = {
        'create_time': 'CreateTime',
        'principal': 'Principal',
        'remote_addr': 'RemoteAddr',
        'type': 'Type'
    }

    def __init__(self, create_time=None, principal=None, remote_addr=None, type=None, _configuration=None):  # noqa: E501
        """Connection - a model defined in Swagger"""  # noqa: E501
        if _configuration is None:
            _configuration = Configuration()
        self._configuration = _configuration

        self._create_time = None
        self._principal = None
        self._remote_addr = None
        self._type = None
        self.discriminator = None

        if create_time is not None:
            self.create_time = create_time
        if principal is not None:
            self.principal = principal
        if remote_addr is not None:
            self.remote_addr = remote_addr
        if type is not None:
            self.type = type

    @property
    def create_time(self):
        """Gets the create_time of this Connection.  # noqa: E501


        :return: The create_time of this Connection.  # noqa: E501
        :rtype: datetime
        """
        return self._create_time

    @create_time.setter
    def create_time(self, create_time):
        """Sets the create_time of this Connection.


        :param create_time: The create_time of this Connection.  # noqa: E501
        :type: datetime
        """

        self._create_time = create_time

    @property
    def principal(self):
        """Gets the principal of this Connection.  # noqa: E501


        :return: The principal of this Connection.  # noqa: E501
        :rtype: str
        """
        return self._principal

    @principal.setter
    def principal(self, principal):
        """Sets the principal of this Connection.


        :param principal: The principal of this Connection.  # noqa: E501
        :type: str
        """

        self._principal = principal

    @property
    def remote_addr(self):
        """Gets the remote_addr of this Connection.  # noqa: E501


        :return: The remote_addr of this Connection.  # noqa: E501
        :rtype: str
        """
        return self._remote_addr

    @remote_addr.setter
    def remote_addr(self, remote_addr):
        """Sets the remote_addr of this Connection.


        :param remote_addr: The remote_addr of this Connection.  # noqa: E501
        :type: str
        """

        self._remote_addr = remote_addr

    @property
    def type(self):
        """Gets the type of this Connection.  # noqa: E501


        :return: The type of this Connection.  # noqa: E501
        :rtype: str
        """
        return self._type

    @type.setter
    def type(self, type):
        """Sets the type of this Connection.


        :param type: The type of this Connection.  # noqa: E501
        :type: str
        """

        self._type = type

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(Connection, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, Connection):
            return False

        return self.to_dict() == other.to_dict()

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        if not isinstance(other, Connection):
            return True

        return self.to_dict() != other.to_dict()
