# coding: utf-8

"""
    DigitalRebar Provision Server

    An RestFUL API-driven Provisioner and DHCP server  # noqa: E501

    OpenAPI spec version: 4.0.0
    Contact: greg@rackn.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import unittest

import drppy_client
from drppy_client.api.a_api import AApi  # noqa: E501
from drppy_client.rest import ApiException


class TestAApi(unittest.TestCase):
    """AApi unit test stubs"""

    def setUp(self):
        self.api = drppy_client.api.a_api.AApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_by(self):
        """Test case for by

        Get a Connection  # noqa: E501
        """
        pass


if __name__ == '__main__':
    unittest.main()
