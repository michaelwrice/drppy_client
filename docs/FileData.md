# FileData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**explode** | **bool** |  | [optional] 
**path** | **str** |  | [optional] 
**sha256_sum** | **str** |  | [optional] 
**source** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


