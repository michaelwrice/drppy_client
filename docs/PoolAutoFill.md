# PoolAutoFill

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**acquire_pool** | **str** |  | [optional] 
**create_parameters** | **dict(str, object)** |  | [optional] 
**max_free** | **int** |  | [optional] 
**min_free** | **int** |  | [optional] 
**return_pool** | **str** |  | [optional] 
**use_auto_fill** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


