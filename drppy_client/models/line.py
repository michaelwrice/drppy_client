# coding: utf-8

"""
    DigitalRebar Provision Server

    # An RestFUL API-driven Provisioner and DHCP server  # noqa: E501

    OpenAPI spec version: 4.14.0
    Contact: support@rackn.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six

from drppy_client.configuration import Configuration


class Line(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'data': 'list[object]',
        'file': 'str',
        'group': 'int',
        'ignore_publish': 'bool',
        'level': 'Level',
        'line': 'int',
        'message': 'str',
        'principal': 'str',
        'seq': 'int',
        'service': 'str',
        'time': 'datetime'
    }

    attribute_map = {
        'data': 'Data',
        'file': 'File',
        'group': 'Group',
        'ignore_publish': 'IgnorePublish',
        'level': 'Level',
        'line': 'Line',
        'message': 'Message',
        'principal': 'Principal',
        'seq': 'Seq',
        'service': 'Service',
        'time': 'Time'
    }

    def __init__(self, data=None, file=None, group=None, ignore_publish=None, level=None, line=None, message=None, principal=None, seq=None, service=None, time=None, _configuration=None):  # noqa: E501
        """Line - a model defined in Swagger"""  # noqa: E501
        if _configuration is None:
            _configuration = Configuration()
        self._configuration = _configuration

        self._data = None
        self._file = None
        self._group = None
        self._ignore_publish = None
        self._level = None
        self._line = None
        self._message = None
        self._principal = None
        self._seq = None
        self._service = None
        self._time = None
        self.discriminator = None

        if data is not None:
            self.data = data
        if file is not None:
            self.file = file
        if group is not None:
            self.group = group
        if ignore_publish is not None:
            self.ignore_publish = ignore_publish
        if level is not None:
            self.level = level
        if line is not None:
            self.line = line
        if message is not None:
            self.message = message
        if principal is not None:
            self.principal = principal
        if seq is not None:
            self.seq = seq
        if service is not None:
            self.service = service
        if time is not None:
            self.time = time

    @property
    def data(self):
        """Gets the data of this Line.  # noqa: E501

        Data is any auxillary data that was captured.  # noqa: E501

        :return: The data of this Line.  # noqa: E501
        :rtype: list[object]
        """
        return self._data

    @data.setter
    def data(self, data):
        """Sets the data of this Line.

        Data is any auxillary data that was captured.  # noqa: E501

        :param data: The data of this Line.  # noqa: E501
        :type: list[object]
        """

        self._data = data

    @property
    def file(self):
        """Gets the file of this Line.  # noqa: E501

        File is the source file that generated the line  # noqa: E501

        :return: The file of this Line.  # noqa: E501
        :rtype: str
        """
        return self._file

    @file.setter
    def file(self, file):
        """Sets the file of this Line.

        File is the source file that generated the line  # noqa: E501

        :param file: The file of this Line.  # noqa: E501
        :type: str
        """

        self._file = file

    @property
    def group(self):
        """Gets the group of this Line.  # noqa: E501

        Group is an abstract number used to group Lines together  # noqa: E501

        :return: The group of this Line.  # noqa: E501
        :rtype: int
        """
        return self._group

    @group.setter
    def group(self, group):
        """Sets the group of this Line.

        Group is an abstract number used to group Lines together  # noqa: E501

        :param group: The group of this Line.  # noqa: E501
        :type: int
        """

        self._group = group

    @property
    def ignore_publish(self):
        """Gets the ignore_publish of this Line.  # noqa: E501

        Should the line be published or not as an event.  # noqa: E501

        :return: The ignore_publish of this Line.  # noqa: E501
        :rtype: bool
        """
        return self._ignore_publish

    @ignore_publish.setter
    def ignore_publish(self, ignore_publish):
        """Sets the ignore_publish of this Line.

        Should the line be published or not as an event.  # noqa: E501

        :param ignore_publish: The ignore_publish of this Line.  # noqa: E501
        :type: bool
        """

        self._ignore_publish = ignore_publish

    @property
    def level(self):
        """Gets the level of this Line.  # noqa: E501


        :return: The level of this Line.  # noqa: E501
        :rtype: Level
        """
        return self._level

    @level.setter
    def level(self, level):
        """Sets the level of this Line.


        :param level: The level of this Line.  # noqa: E501
        :type: Level
        """

        self._level = level

    @property
    def line(self):
        """Gets the line of this Line.  # noqa: E501

        Line is the line number of the line that generated the line.  # noqa: E501

        :return: The line of this Line.  # noqa: E501
        :rtype: int
        """
        return self._line

    @line.setter
    def line(self, line):
        """Sets the line of this Line.

        Line is the line number of the line that generated the line.  # noqa: E501

        :param line: The line of this Line.  # noqa: E501
        :type: int
        """

        self._line = line

    @property
    def message(self):
        """Gets the message of this Line.  # noqa: E501

        Message is the message that was logged.  # noqa: E501

        :return: The message of this Line.  # noqa: E501
        :rtype: str
        """
        return self._message

    @message.setter
    def message(self, message):
        """Sets the message of this Line.

        Message is the message that was logged.  # noqa: E501

        :param message: The message of this Line.  # noqa: E501
        :type: str
        """

        self._message = message

    @property
    def principal(self):
        """Gets the principal of this Line.  # noqa: E501

        Principal is the user or system that caused the log line to be emitted  # noqa: E501

        :return: The principal of this Line.  # noqa: E501
        :rtype: str
        """
        return self._principal

    @principal.setter
    def principal(self, principal):
        """Sets the principal of this Line.

        Principal is the user or system that caused the log line to be emitted  # noqa: E501

        :param principal: The principal of this Line.  # noqa: E501
        :type: str
        """

        self._principal = principal

    @property
    def seq(self):
        """Gets the seq of this Line.  # noqa: E501

        Seq is the sequence number that the Line was emitted in. Sequence numbers are globally unique.  # noqa: E501

        :return: The seq of this Line.  # noqa: E501
        :rtype: int
        """
        return self._seq

    @seq.setter
    def seq(self, seq):
        """Sets the seq of this Line.

        Seq is the sequence number that the Line was emitted in. Sequence numbers are globally unique.  # noqa: E501

        :param seq: The seq of this Line.  # noqa: E501
        :type: int
        """

        self._seq = seq

    @property
    def service(self):
        """Gets the service of this Line.  # noqa: E501

        Service is the name of the log.  # noqa: E501

        :return: The service of this Line.  # noqa: E501
        :rtype: str
        """
        return self._service

    @service.setter
    def service(self, service):
        """Sets the service of this Line.

        Service is the name of the log.  # noqa: E501

        :param service: The service of this Line.  # noqa: E501
        :type: str
        """

        self._service = service

    @property
    def time(self):
        """Gets the time of this Line.  # noqa: E501

        Time is when the Line was created.  # noqa: E501

        :return: The time of this Line.  # noqa: E501
        :rtype: datetime
        """
        return self._time

    @time.setter
    def time(self, time):
        """Sets the time of this Line.

        Time is when the Line was created.  # noqa: E501

        :param time: The time of this Line.  # noqa: E501
        :type: datetime
        """

        self._time = time

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(Line, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, Line):
            return False

        return self.to_dict() == other.to_dict()

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        if not isinstance(other, Line):
            return True

        return self.to_dict() != other.to_dict()
