# Endpoint

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**actions** | [**list[ElementAction]**](ElementAction.md) | Actions is the list of actions to take to make the endpoint match the version sets on in the endpoint object. | [optional] 
**apply** | **bool** | Apply toggles whether the manager should update the endpoint. | [optional] 
**arch** | **str** | Arch is the arch of the endpoint - Golang arch format. | [optional] 
**available** | **bool** | Available tracks whether or not the model passed validation. | [optional] 
**bundle** | **str** | Bundle tracks the name of the store containing this object. This field is read-only, and cannot be changed via the API. | [optional] 
**components** | [**list[Element]**](Element.md) | Components is the list of ContentPackages and PluginProviders installed and their versions | [optional] 
**connection_status** | **str** | ConnectionStatus reflects the manager&#39;s state of interaction with the endpoint | [optional] 
**drpux_version** | **str** | DRPUXVersion is the version of the ux installed on the endpoint. | [optional] 
**drp_version** | **str** | DRPVersion is the version of the drp endpoint running. | [optional] 
**description** | **str** | Description is a string for providing a simple description | [optional] 
**documentation** | **str** | Documentation is a string for providing additional in depth information. | [optional] 
**endpoint** | **str** | Endpoint tracks the owner of the object among DRP endpoints | [optional] 
**errors** | **list[str]** | If there are any errors in the validation process, they will be available here. | [optional] 
**_global** | **dict(str, object)** | Global is the Parameters of the global profile. | [optional] 
**ha_id** | **str** | HaId is the HaId of the endpoint | [optional] 
**id** | **str** | Id is the name of the DRP endpoint this should match the HA pair&#39;s ID or the DRP ID of a single node. | [optional] 
**os** | **str** | Os is the os of the endpoint - Golang os format. | [optional] 
**params** | **dict(str, object)** | Params holds the access parameters - these should be secure parameters. They are: manager/username manager/password manager/url | [optional] 
**plugins** | [**list[Plugin]**](Plugin.md) | Plugins is the list of Plugins configured on the endpoint. | [optional] 
**prefs** | **dict(str, str)** | Prefs is the value of all the prefs on the endpoint. | [optional] 
**read_only** | **bool** | ReadOnly tracks if the store for this object is read-only. This flag is informational, and cannot be changed via the API. | [optional] 
**validated** | **bool** | Validated tracks whether or not the model has been validated. | [optional] 
**version_set** | **str** | VersionSet - Deprecated - was a single version set. This should be specified within the VersionSets list | [optional] 
**version_sets** | **list[str]** | VersionSets replaces VersionSet - code processes both This is the list of version sets to apply.  These are merged with the first in the list having priority over later elements in the list. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


