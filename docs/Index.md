# Index

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**regex** | **bool** | Regex indicates whether you can use the Re filter with this index | [optional] 
**type** | **str** | Type gives you a rough idea of how the string used to query this index should be formatted. | [optional] 
**unique** | **bool** | Unique tells you whether there can be multiple entries in the index for the same key that refer to different items. | [optional] 
**unordered** | **bool** | Unordered tells you whether this index cannot be sorted. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


