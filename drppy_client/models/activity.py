# coding: utf-8

"""
    DigitalRebar Provision Server

    # An RestFUL API-driven Provisioner and DHCP server  # noqa: E501

    OpenAPI spec version: 4.14.0
    Contact: support@rackn.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six

from drppy_client.configuration import Configuration


class Activity(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'arch': 'str',
        'available': 'bool',
        'bundle': 'str',
        'cloud': 'str',
        'context': 'str',
        'count': 'int',
        'created_at': 'datetime',
        'created_by': 'str',
        'deleted': 'bool',
        'endpoint': 'str',
        'errors': 'list[str]',
        'fingerprint': 'str',
        'id': 'str',
        'identity': 'str',
        'last_modified_at': 'datetime',
        'last_modified_by': 'str',
        'os': 'str',
        'object_type': 'str',
        'platform': 'str',
        'read_only': 'bool',
        'span': 'str',
        'type': 'str',
        'validated': 'bool'
    }

    attribute_map = {
        'arch': 'Arch',
        'available': 'Available',
        'bundle': 'Bundle',
        'cloud': 'Cloud',
        'context': 'Context',
        'count': 'Count',
        'created_at': 'CreatedAt',
        'created_by': 'CreatedBy',
        'deleted': 'Deleted',
        'endpoint': 'Endpoint',
        'errors': 'Errors',
        'fingerprint': 'Fingerprint',
        'id': 'Id',
        'identity': 'Identity',
        'last_modified_at': 'LastModifiedAt',
        'last_modified_by': 'LastModifiedBy',
        'os': 'OS',
        'object_type': 'ObjectType',
        'platform': 'Platform',
        'read_only': 'ReadOnly',
        'span': 'Span',
        'type': 'Type',
        'validated': 'Validated'
    }

    def __init__(self, arch=None, available=None, bundle=None, cloud=None, context=None, count=None, created_at=None, created_by=None, deleted=None, endpoint=None, errors=None, fingerprint=None, id=None, identity=None, last_modified_at=None, last_modified_by=None, os=None, object_type=None, platform=None, read_only=None, span=None, type=None, validated=None, _configuration=None):  # noqa: E501
        """Activity - a model defined in Swagger"""  # noqa: E501
        if _configuration is None:
            _configuration = Configuration()
        self._configuration = _configuration

        self._arch = None
        self._available = None
        self._bundle = None
        self._cloud = None
        self._context = None
        self._count = None
        self._created_at = None
        self._created_by = None
        self._deleted = None
        self._endpoint = None
        self._errors = None
        self._fingerprint = None
        self._id = None
        self._identity = None
        self._last_modified_at = None
        self._last_modified_by = None
        self._os = None
        self._object_type = None
        self._platform = None
        self._read_only = None
        self._span = None
        self._type = None
        self._validated = None
        self.discriminator = None

        if arch is not None:
            self.arch = arch
        if available is not None:
            self.available = available
        if bundle is not None:
            self.bundle = bundle
        if cloud is not None:
            self.cloud = cloud
        if context is not None:
            self.context = context
        if count is not None:
            self.count = count
        if created_at is not None:
            self.created_at = created_at
        if created_by is not None:
            self.created_by = created_by
        if deleted is not None:
            self.deleted = deleted
        if endpoint is not None:
            self.endpoint = endpoint
        if errors is not None:
            self.errors = errors
        if fingerprint is not None:
            self.fingerprint = fingerprint
        if id is not None:
            self.id = id
        if identity is not None:
            self.identity = identity
        if last_modified_at is not None:
            self.last_modified_at = last_modified_at
        if last_modified_by is not None:
            self.last_modified_by = last_modified_by
        if os is not None:
            self.os = os
        if object_type is not None:
            self.object_type = object_type
        if platform is not None:
            self.platform = platform
        if read_only is not None:
            self.read_only = read_only
        if span is not None:
            self.span = span
        if type is not None:
            self.type = type
        if validated is not None:
            self.validated = validated

    @property
    def arch(self):
        """Gets the arch of this Activity.  # noqa: E501

        Arch is the architecture of the machine e.g. amd64  # noqa: E501

        :return: The arch of this Activity.  # noqa: E501
        :rtype: str
        """
        return self._arch

    @arch.setter
    def arch(self, arch):
        """Sets the arch of this Activity.

        Arch is the architecture of the machine e.g. amd64  # noqa: E501

        :param arch: The arch of this Activity.  # noqa: E501
        :type: str
        """

        self._arch = arch

    @property
    def available(self):
        """Gets the available of this Activity.  # noqa: E501

        Available tracks whether or not the model passed validation.  # noqa: E501

        :return: The available of this Activity.  # noqa: E501
        :rtype: bool
        """
        return self._available

    @available.setter
    def available(self, available):
        """Sets the available of this Activity.

        Available tracks whether or not the model passed validation.  # noqa: E501

        :param available: The available of this Activity.  # noqa: E501
        :type: bool
        """

        self._available = available

    @property
    def bundle(self):
        """Gets the bundle of this Activity.  # noqa: E501

        Bundle tracks the name of the store containing this object. This field is read-only, and cannot be changed via the API.  # noqa: E501

        :return: The bundle of this Activity.  # noqa: E501
        :rtype: str
        """
        return self._bundle

    @bundle.setter
    def bundle(self, bundle):
        """Sets the bundle of this Activity.

        Bundle tracks the name of the store containing this object. This field is read-only, and cannot be changed via the API.  # noqa: E501

        :param bundle: The bundle of this Activity.  # noqa: E501
        :type: str
        """

        self._bundle = bundle

    @property
    def cloud(self):
        """Gets the cloud of this Activity.  # noqa: E501

        Cloud is the cloud it is running in if set.  # noqa: E501

        :return: The cloud of this Activity.  # noqa: E501
        :rtype: str
        """
        return self._cloud

    @cloud.setter
    def cloud(self, cloud):
        """Sets the cloud of this Activity.

        Cloud is the cloud it is running in if set.  # noqa: E501

        :param cloud: The cloud of this Activity.  # noqa: E501
        :type: str
        """

        self._cloud = cloud

    @property
    def context(self):
        """Gets the context of this Activity.  # noqa: E501

        Context is the context of the machine e.g. \"\" or drpcli-runner  # noqa: E501

        :return: The context of this Activity.  # noqa: E501
        :rtype: str
        """
        return self._context

    @context.setter
    def context(self, context):
        """Sets the context of this Activity.

        Context is the context of the machine e.g. \"\" or drpcli-runner  # noqa: E501

        :param context: The context of this Activity.  # noqa: E501
        :type: str
        """

        self._context = context

    @property
    def count(self):
        """Gets the count of this Activity.  # noqa: E501

        Number of times for this entry  # noqa: E501

        :return: The count of this Activity.  # noqa: E501
        :rtype: int
        """
        return self._count

    @count.setter
    def count(self, count):
        """Sets the count of this Activity.

        Number of times for this entry  # noqa: E501

        :param count: The count of this Activity.  # noqa: E501
        :type: int
        """

        self._count = count

    @property
    def created_at(self):
        """Gets the created_at of this Activity.  # noqa: E501

        CreatedAt is the time that this object was created.  # noqa: E501

        :return: The created_at of this Activity.  # noqa: E501
        :rtype: datetime
        """
        return self._created_at

    @created_at.setter
    def created_at(self, created_at):
        """Sets the created_at of this Activity.

        CreatedAt is the time that this object was created.  # noqa: E501

        :param created_at: The created_at of this Activity.  # noqa: E501
        :type: datetime
        """

        self._created_at = created_at

    @property
    def created_by(self):
        """Gets the created_by of this Activity.  # noqa: E501

        CreatedBy stores the value of the user that created this object. Note: This value is stored ONLY if the object was created by a user which means that `currentUserName` needs to be populated in the authBlob  # noqa: E501

        :return: The created_by of this Activity.  # noqa: E501
        :rtype: str
        """
        return self._created_by

    @created_by.setter
    def created_by(self, created_by):
        """Sets the created_by of this Activity.

        CreatedBy stores the value of the user that created this object. Note: This value is stored ONLY if the object was created by a user which means that `currentUserName` needs to be populated in the authBlob  # noqa: E501

        :param created_by: The created_by of this Activity.  # noqa: E501
        :type: str
        """

        self._created_by = created_by

    @property
    def deleted(self):
        """Gets the deleted of this Activity.  # noqa: E501

        Deleted indicates if the entry was deleted.  # noqa: E501

        :return: The deleted of this Activity.  # noqa: E501
        :rtype: bool
        """
        return self._deleted

    @deleted.setter
    def deleted(self, deleted):
        """Sets the deleted of this Activity.

        Deleted indicates if the entry was deleted.  # noqa: E501

        :param deleted: The deleted of this Activity.  # noqa: E501
        :type: bool
        """

        self._deleted = deleted

    @property
    def endpoint(self):
        """Gets the endpoint of this Activity.  # noqa: E501

        Endpoint tracks the owner of the object among DRP endpoints  # noqa: E501

        :return: The endpoint of this Activity.  # noqa: E501
        :rtype: str
        """
        return self._endpoint

    @endpoint.setter
    def endpoint(self, endpoint):
        """Sets the endpoint of this Activity.

        Endpoint tracks the owner of the object among DRP endpoints  # noqa: E501

        :param endpoint: The endpoint of this Activity.  # noqa: E501
        :type: str
        """

        self._endpoint = endpoint

    @property
    def errors(self):
        """Gets the errors of this Activity.  # noqa: E501

        If there are any errors in the validation process, they will be available here.  # noqa: E501

        :return: The errors of this Activity.  # noqa: E501
        :rtype: list[str]
        """
        return self._errors

    @errors.setter
    def errors(self, errors):
        """Sets the errors of this Activity.

        If there are any errors in the validation process, they will be available here.  # noqa: E501

        :param errors: The errors of this Activity.  # noqa: E501
        :type: list[str]
        """

        self._errors = errors

    @property
    def fingerprint(self):
        """Gets the fingerprint of this Activity.  # noqa: E501

        Fingerprint indicates a unique machine specific identifier  # noqa: E501

        :return: The fingerprint of this Activity.  # noqa: E501
        :rtype: str
        """
        return self._fingerprint

    @fingerprint.setter
    def fingerprint(self, fingerprint):
        """Sets the fingerprint of this Activity.

        Fingerprint indicates a unique machine specific identifier  # noqa: E501

        :param fingerprint: The fingerprint of this Activity.  # noqa: E501
        :type: str
        """

        self._fingerprint = fingerprint

    @property
    def id(self):
        """Gets the id of this Activity.  # noqa: E501

        Id of the activity entry.  # noqa: E501

        :return: The id of this Activity.  # noqa: E501
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this Activity.

        Id of the activity entry.  # noqa: E501

        :param id: The id of this Activity.  # noqa: E501
        :type: str
        """

        self._id = id

    @property
    def identity(self):
        """Gets the identity of this Activity.  # noqa: E501

        Identity is the uuid/identity of the record  # noqa: E501

        :return: The identity of this Activity.  # noqa: E501
        :rtype: str
        """
        return self._identity

    @identity.setter
    def identity(self, identity):
        """Sets the identity of this Activity.

        Identity is the uuid/identity of the record  # noqa: E501

        :param identity: The identity of this Activity.  # noqa: E501
        :type: str
        """

        self._identity = identity

    @property
    def last_modified_at(self):
        """Gets the last_modified_at of this Activity.  # noqa: E501

        LastModifiedAt is the time that this object was last modified.  # noqa: E501

        :return: The last_modified_at of this Activity.  # noqa: E501
        :rtype: datetime
        """
        return self._last_modified_at

    @last_modified_at.setter
    def last_modified_at(self, last_modified_at):
        """Sets the last_modified_at of this Activity.

        LastModifiedAt is the time that this object was last modified.  # noqa: E501

        :param last_modified_at: The last_modified_at of this Activity.  # noqa: E501
        :type: datetime
        """

        self._last_modified_at = last_modified_at

    @property
    def last_modified_by(self):
        """Gets the last_modified_by of this Activity.  # noqa: E501

        LastModifiedBy stores the value of the user that last modified this object. NOTE: This value is populated ONLY if the object was modified by a user which means any actions done using machine tokens will not get tracked  # noqa: E501

        :return: The last_modified_by of this Activity.  # noqa: E501
        :rtype: str
        """
        return self._last_modified_by

    @last_modified_by.setter
    def last_modified_by(self, last_modified_by):
        """Sets the last_modified_by of this Activity.

        LastModifiedBy stores the value of the user that last modified this object. NOTE: This value is populated ONLY if the object was modified by a user which means any actions done using machine tokens will not get tracked  # noqa: E501

        :param last_modified_by: The last_modified_by of this Activity.  # noqa: E501
        :type: str
        """

        self._last_modified_by = last_modified_by

    @property
    def os(self):
        """Gets the os of this Activity.  # noqa: E501

        OS is the operating system of the machine - could be off  # noqa: E501

        :return: The os of this Activity.  # noqa: E501
        :rtype: str
        """
        return self._os

    @os.setter
    def os(self, os):
        """Sets the os of this Activity.

        OS is the operating system of the machine - could be off  # noqa: E501

        :param os: The os of this Activity.  # noqa: E501
        :type: str
        """

        self._os = os

    @property
    def object_type(self):
        """Gets the object_type of this Activity.  # noqa: E501

        Object Type  # noqa: E501

        :return: The object_type of this Activity.  # noqa: E501
        :rtype: str
        """
        return self._object_type

    @object_type.setter
    def object_type(self, object_type):
        """Sets the object_type of this Activity.

        Object Type  # noqa: E501

        :param object_type: The object_type of this Activity.  # noqa: E501
        :type: str
        """

        self._object_type = object_type

    @property
    def platform(self):
        """Gets the platform of this Activity.  # noqa: E501

        Platform is type of entry Usually: meta, physical, virtual, container  # noqa: E501

        :return: The platform of this Activity.  # noqa: E501
        :rtype: str
        """
        return self._platform

    @platform.setter
    def platform(self, platform):
        """Sets the platform of this Activity.

        Platform is type of entry Usually: meta, physical, virtual, container  # noqa: E501

        :param platform: The platform of this Activity.  # noqa: E501
        :type: str
        """

        self._platform = platform

    @property
    def read_only(self):
        """Gets the read_only of this Activity.  # noqa: E501

        ReadOnly tracks if the store for this object is read-only. This flag is informational, and cannot be changed via the API.  # noqa: E501

        :return: The read_only of this Activity.  # noqa: E501
        :rtype: bool
        """
        return self._read_only

    @read_only.setter
    def read_only(self, read_only):
        """Sets the read_only of this Activity.

        ReadOnly tracks if the store for this object is read-only. This flag is informational, and cannot be changed via the API.  # noqa: E501

        :param read_only: The read_only of this Activity.  # noqa: E501
        :type: bool
        """

        self._read_only = read_only

    @property
    def span(self):
        """Gets the span of this Activity.  # noqa: E501

        Span is the time window  # noqa: E501

        :return: The span of this Activity.  # noqa: E501
        :rtype: str
        """
        return self._span

    @span.setter
    def span(self, span):
        """Sets the span of this Activity.

        Span is the time window  # noqa: E501

        :param span: The span of this Activity.  # noqa: E501
        :type: str
        """

        self._span = span

    @property
    def type(self):
        """Gets the type of this Activity.  # noqa: E501

        Type of the activity (from RawModel days) Should be set to activities if present  # noqa: E501

        :return: The type of this Activity.  # noqa: E501
        :rtype: str
        """
        return self._type

    @type.setter
    def type(self, type):
        """Sets the type of this Activity.

        Type of the activity (from RawModel days) Should be set to activities if present  # noqa: E501

        :param type: The type of this Activity.  # noqa: E501
        :type: str
        """

        self._type = type

    @property
    def validated(self):
        """Gets the validated of this Activity.  # noqa: E501

        Validated tracks whether or not the model has been validated.  # noqa: E501

        :return: The validated of this Activity.  # noqa: E501
        :rtype: bool
        """
        return self._validated

    @validated.setter
    def validated(self, validated):
        """Sets the validated of this Activity.

        Validated tracks whether or not the model has been validated.  # noqa: E501

        :param validated: The validated of this Activity.  # noqa: E501
        :type: bool
        """

        self._validated = validated

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(Activity, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, Activity):
            return False

        return self.to_dict() == other.to_dict()

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        if not isinstance(other, Activity):
            return True

        return self.to_dict() != other.to_dict()
