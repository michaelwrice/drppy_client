# Classifier

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_continue** | **bool** |  | [optional] 
**icon** | **str** |  | [optional] 
**no_append** | **bool** |  | [optional] 
**params** | **dict(str, object)** |  | [optional] 
**placeholder** | **str** |  | [optional] 
**regex** | **str** |  | [optional] 
**test** | **str** |  | [optional] 
**title** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


